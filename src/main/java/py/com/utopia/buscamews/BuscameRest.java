/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.utopia.buscamews;

import com.google.gson.Gson;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import static javax.ws.rs.HttpMethod.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.ws.rs.POST;
import javax.ws.rs.core.MediaType;
import org.json.JSONObject;
import py.com.utopia.buscamews.Buscame;
import py.com.utopia.buscamews.dto.BuscameDTO;
/**
 * REST Web Service
 *
 * @author stanichs
 */
@Path("buscame")
public class BuscameRest {

    @Context
    private UriInfo context;

    /**
     * Creates a new instance of BuscameRest
     */
    public BuscameRest() {
    }

    /**
     * Retrieves representation of an instance of py.com.utopia.buscamews.BuscameRest
     * @return an instance of java.lang.String
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public String getJson() {
        //TODO return proper representation object
       return "{\"Estado\":\"Buscame en POST\"}";
    }

    /**
     * PUT method for updating or creating an instance of BuscameRest
     * @param content representation for the resource
     */
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String putJson(String content) {
        Buscame bus = new Buscame();
            Gson gson = new Gson();
            JSONObject req = null;
            
            String user ="vacio";
            String pass = "vacio";
            String tipo = "full";
            String cedula= "1";
            
            
            req = new JSONObject(content);
            user = req.getString("usuario");
            pass = req.getString("password");
            cedula = req.getString("cedula");
            try { tipo = req.getString("tipo");}catch(Exception e){tipo = "full";}
        BuscameDTO res = bus.buscameDatos(user, pass, cedula,tipo);
        
        return gson.toJson(res);
    }
    
    @POST
    @Path("/persona")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String buscameDatos(String content) {
        Buscame bus = new Buscame();
            Gson gson = new Gson();
            JSONObject req = null;
            
            String user ="vacio";
            String pass = "vacio";
            String cedula= "1";
            String tipo = "full";
            
            req = new JSONObject(content);
            user = req.getString("usuario");
            pass = req.getString("password");
            cedula = req.getString("cedula");
             //que kk json
            try { tipo = req.getString("tipo");}catch(Exception e){tipo = "full";}
        BuscameDTO res = bus.buscameDatos(user, pass, cedula, tipo);
        
        return gson.toJson(res);
    }
    
    
     @POST
    @Path("/telefono")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String buscametelefono(String content) {
        Buscame bus = new Buscame();
            Gson gson = new Gson();
            JSONObject req = null;
            
            String user ="vacio";
            String pass = "vacio";
            String telefono= "1";
            
            
            req = new JSONObject(content);
            user = req.getString("usuario");
            pass = req.getString("password");
            telefono = req.getString("cedula");
        BuscameDTO res = bus.buscameelefono(user, pass, telefono);
        
        return gson.toJson(res);
    }
    
}
