/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.utopia.buscamews.dto;

/**
 *
 * @author sergio
 * @author Fernando Invernizzi <ferinver92@gmail.com>
 */
public class Set {

    private String cedula;
    private String dv;
    private String contribuyente;
    private String rucAnterior;
    private String fechaActualizacion;
    private String tipo;
    private String estado;
    private String tipoContribuyente;
    private String tipoObligacion;
    private String impuestosRegistrados;
    private String rubro;
    private String direccionEstableciemiento;
    private String otros;

    public Set() {
    }

    /**
     * @return the cedula
     */
    public String getCedula() {
        return cedula;
    }

    /**
     * @param cedula the cedula to set
     */
    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    /**
     * @return the dv
     */
    public String getDv() {
        return dv;
    }

    /**
     * @param dv the dv to set
     */
    public void setDv(String dv) {
        this.dv = dv;
    }

    /**
     * @return the contribuyente
     */
    public String getContribuyente() {
        return contribuyente;
    }

    /**
     * @param contribuyente the contribuyente to set
     */
    public void setContribuyente(String contribuyente) {
        this.contribuyente = contribuyente;
    }

    /**
     * @return the rucAnterior
     */
    public String getRucAnterior() {
        return rucAnterior;
    }

    /**
     * @param rucAnterior the rucAnterior to set
     */
    public void setRucAnterior(String rucAnterior) {
        this.rucAnterior = rucAnterior;
    }

    /**
     * @return the fechaActualizacion
     */
    public String getFechaActualizacion() {
        return fechaActualizacion;
    }

    /**
     * @param fechaActualizacion the fechaActualizacion to set
     */
    public void setFechaActualizacion(String fechaActualizacion) {
        this.fechaActualizacion = fechaActualizacion;
    }

    /**
     * @return the tipo
     */
    public String getTipo() {
        return tipo;
    }

    /**
     * @param tipo the tipo to set
     */
    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    /**
     * @return the estado
     */
    public String getEstado() {
        return estado;
    }

    /**
     * @param estado the estado to set
     */
    public void setEstado(String estado) {
        this.estado = estado;
    }

    /**
     * @return the tipoContribuyente
     */
    public String getTipoContribuyente() {
        return tipoContribuyente;
    }

    /**
     * @param tipoContribuyente the tipoContribuyente to set
     */
    public void setTipoContribuyente(String tipoContribuyente) {
        this.tipoContribuyente = tipoContribuyente;
    }

    /**
     * @return the tipoObligacion
     */
    public String getTipoObligacion() {
        return tipoObligacion;
    }

    /**
     * @param tipoObligacion the tipoObligacion to set
     */
    public void setTipoObligacion(String tipoObligacion) {
        this.tipoObligacion = tipoObligacion;
    }

    /**
     * @return the impuestosRegistrados
     */
    public String getImpuestosRegistrados() {
        return impuestosRegistrados;
    }

    /**
     * @param impuestosRegistrados the impuestosRegistrados to set
     */
    public void setImpuestosRegistrados(String impuestosRegistrados) {
        this.impuestosRegistrados = impuestosRegistrados;
    }

    /**
     * @return the rubro
     */
    public String getRubro() {
        return rubro;
    }

    /**
     * @param rubro the rubro to set
     */
    public void setRubro(String rubro) {
        this.rubro = rubro;
    }

    /**
     * @return the direccionEstableciemiento
     */
    public String getDireccionEstableciemiento() {
        return direccionEstableciemiento;
    }

    /**
     * @param direccionEstableciemiento the direccionEstableciemiento to set
     */
    public void setDireccionEstableciemiento(String direccionEstableciemiento) {
        this.direccionEstableciemiento = direccionEstableciemiento;
    }

    /**
     * @return the otros
     */
    public String getOtros() {
        return otros;
    }

    /**
     * @param otros the otros to set
     */
    public void setOtros(String otros) {
        this.otros = otros;
    }

 
}
