/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.utopia.buscamews.dto;

import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import java.io.FileOutputStream;
import java.io.IOException;


/**
 *
 * @author stanichs
 */
public class GeneracionPDF {
      /** Path to the resulting PDF file. */
    public static final String RESULT
        = "hello_mirrored_top.pdf";
 
    /**
     * Creates a PDF file: hello_mirrored_margins.pdf
     * @param    args    no arguments needed
     */
    byte[] generacionPDF(BuscameDTO dto) throws DocumentException, IOException {
        // step 1
        Document document = new Document();
        // step 2
        PdfWriter.getInstance(document, new FileOutputStream(RESULT));
        // setting page size, margins and mirrered margins
        document.setPageSize(PageSize.A5);
        document.setMargins(36, 72, 108, 180);
        document.setMarginMirroringTopBottom(true);
        // step 3
        document.open();
        
        
         PdfPTable table = new PdfPTable(2);
        
        
        return null;
    }
    
    
    public static void main(String[] args)
        throws DocumentException, IOException {
        // step 1
        Document document = new Document();
        // step 2
        PdfWriter.getInstance(document, new FileOutputStream(RESULT));
        // setting page size, margins and mirrered margins
        document.setPageSize(PageSize.A4);
        document.setMargins(5, 80, 108, 180);
        document.setMarginMirroringTopBottom(true);
        // step 3
        document.open();
        // step 4
        document.add(new Paragraph("PROCESO DE BUSQUEDA DE PERSONA"));
        Paragraph paragraph = new Paragraph();
        paragraph.setAlignment(Element.ALIGN_JUSTIFIED);
       
        paragraph.add("");
        
        document.add(paragraph);
        document.add(new Paragraph("texto de prueba"));
        document.add( Chunk.NEWLINE );
        
        // a table with three columns
        PdfPTable table = new PdfPTable(2);
        // the cell object
        PdfPCell cell;
        // we add a cell with colspan 3
        cell = new PdfPCell(new Phrase("DATOS DE LA PERSONA"));
        cell.setColspan(2);
        table.addCell(cell);
        // now we add a cell with rowspan 2
        // we add the four remaining cells with addCell()
       
        table.addCell("NOMBRE: "+""+"APELLIDOS: ");
        table.addCell("EDAD: ");
        table.addCell("SEXO: ");
        table.addCell("NACIONALIDAD: "+ "" + "NACIONALIDAD ORIGEN: ");
        table.addCell("NACIONALIDAD ORIGEN: ");
        table.addCell("ESTADO CIVIL: ");
        
        document.add(table);
         document.add( Chunk.NEWLINE );
        
        // a table with three columns
        table = new PdfPTable(2);
        // the cell object
       
        // we add a cell with colspan 3
        cell = new PdfPCell(new Phrase("DOMILICIO Y PROFESION DE LA PERSONA"));
        cell.setColspan(2);
        table.addCell(cell);
        // now we add a cell with rowspan 2
        // we add the four remaining cells with addCell()
        table.addCell("LUGAR: " );
        table.addCell("DOMILICIO: ");
        table.addCell("PROFESION: ");
        table.addCell("SEXO: ");
        table.addCell("BARRIO: ");
        table.addCell("CIUDAD:");
        
        
        document.add(table);
        
      document.add( Chunk.NEWLINE );
        
        // a table with three columns
        table = new PdfPTable(2);
        // the cell object
       
        // we add a cell with colspan 3
        cell = new PdfPCell(new Phrase("DATOS DE LA CI"));
        cell.setColspan(2);
        table.addCell(cell);
        // now we add a cell with rowspan 2
        // we add the four remaining cells with addCell()
        table.addCell("DOCUMENTO: " +""+ "TIPO: ");
        table.addCell("FECHA EMISION: " );
        table.addCell("FECHA GRABACION: ");
        table.addCell("FECHA VENC: ");
        table.addCell("FECHA VENC APROX: ");
        table.addCell("DOCUMENTO EXTRANJERO: ");
        
        
        document.add(table);
        
          document.add( Chunk.NEWLINE );
        
          table = new PdfPTable(2);
        // the cell object
       
        // we add a cell with colspan 3
        cell = new PdfPCell(new Phrase("TELEFONOS PERSONA"));
        cell.setColspan(2);
        table.addCell(cell);
        // now we add a cell with rowspan 2
        // we add the four remaining cells with addCell()
        table.addCell("TELEFONO PRINCIPAL: ");
        table.addCell("NRO. CUENTA: " );
        table.addCell("OPERADORA: ");
        table.addCell("BILLING: ");
        table.addCell("NOMBRES: "+"APELLIDOS: ");
        table.addCell("FECHA ACTIVACION: ");
        /*
    private String operador;
    private String nroCuenta;
    private String billing;
    private String nombres;
    private String apellidos;
    private String fechaActivacion;*/
        
        document.add(table);
        
          document.add( Chunk.NEWLINE );
        
          
          
                // we add a cell with colspan 3
        cell = new PdfPCell(new Phrase("DATOS FAMILIARES"));
        cell.setColspan(2);
        table.addCell(cell);
        // now we add a cell with rowspan 2
        // we add the four remaining cells with addCell()
        table.addCell("CI PADRE: " );
        table.addCell("NOMBRE APELLIDO PADRE: " );
        table.addCell("CI MADRE:  ");
        table.addCell("NOMBRE APELLIDO MADRE: ");
        table.addCell("CI CONYUGUE:  ");
        table.addCell("NOMBRE APELLIDO CONYUGUE: ");
        /*String nroDocPadre;
    String nombrePadre;
    String apellidoPadre;
    String nroDocMadre;
    String nombreMadre;
    String apellidoMadre;
    String nroDocConyuge;
    String nombreConyuge;
    String apellidoConyuge;*/
        
         table.addCell("CI HERMANO:  ");
        table.addCell("NOMBRE APELLIDO HERMANO: ");
        
        document.add(table);
        
          document.add( Chunk.NEWLINE );  
          
          
          
              // a table with three columns
        table = new PdfPTable(2);
        // the cell object
       
        // we add a cell with colspan 3
        cell = new PdfPCell(new Phrase("DATOS TSJE"));
        cell.setColspan(2);
        table.addCell(cell);
        // now we add a cell with rowspan 2
        // we add the four remaining cells with addCell()
        table.addCell("DEPARTAMENTO: " );
        table.addCell("DISTRITO: " );
        table.addCell("ZONA");
        table.addCell("LOCAL");
        table.addCell("FECHA INSCRIPCION: ");
        
        /*   private String departamento;
    private String distrito;
    private String zona;
    private String local;
    private String fechaInscripcion;
*/
        document.add(table);
        
          document.add( Chunk.NEWLINE );
          
          
          
                  // a table with three columns
        table = new PdfPTable(2);
        // the cell object
       
        // we add a cell with colspan 3
        cell = new PdfPCell(new Phrase("DATOS SET"));
        cell.setColspan(2);
        table.addCell(cell);
        // now we add a cell with rowspan 2
        // we add the four remaining cells with addCell()
        table.addCell("CEDULA: " +"-"+ "DV" );
        table.addCell("CONTRIBUYENTE: " );
        table.addCell("RUC ANTERIOR");
        table.addCell("FECHA ACTUALIZACION: ");
        
        /*      private String cedula;
    private String dv;
    private String contribuyente;
    private String rucAnterior;
    private String fechaActualizacion;
*/
        document.add(table);
        
          document.add( Chunk.NEWLINE );
          
          
          
           document.add(table);
        
          document.add( Chunk.NEWLINE );
          
          
          
                  // a table with three columns
        table = new PdfPTable(2);
        // the cell object
       
        // we add a cell with colspan 3
        cell = new PdfPCell(new Phrase("DATOS IPS"));
        cell.setColspan(2);
        table.addCell(cell);
        // now we add a cell with rowspan 2
        // we add the four remaining cells with addCell()
        table.addCell("CEDULA: "  );
        table.addCell("TIPO ASEGURADO: " );
        table.addCell("BENEFICIARIOS ACTIVOS:");
        table.addCell("NRO PATRONAL: ");
        table.addCell("EMPLEADOR: ");
        table.addCell("ESTADO EMPLEADOR: ");
        table.addCell("MES APORTE: ");
        table.addCell("FECHA ACTUALIZACION: ");
        table.addCell("FECHA ULTIMO REGISTRO: ");
        
        
        /*        String cedula;
    String tipoAsegurado;
    String beneficiariosActivos;
    String nroPatronal;
    String empleador;
    String estadoEmpleador;
    String mesesAporte;
    String fechaActualizacion;
*/
        document.add(table);
        
          document.add( Chunk.NEWLINE );
          
        // step 5
        document.close();
    }
}


/*    private String nroDocumento;V
    private String tipo;V
    private String nombre;V
    private String apellidos;V
    private String fechaNac;V
    private String edad;V
    private String nacionalidad;V
    private String nacionalidadOrigen;V
    private String lugar;V
    private String profesion;V
    private String sexo;V
    private String estadoCiv;V
    private String domicilio;V
    private String barrio;
    private String ciudad;
    private String fechaEmisionCi;
    private String fechaGrabCi;
    private String fechaVencCi;
    private String fechaAproxVto;
    private String nroDocExtranjero;
    private String telefono;
    private DatosFamiliares familiares;
    private List<PosiblesHermanos> hermanos;
    private List<PadronTsje> padron;
    private List<TelefonosDeContacto> telefonos;
    private List<Set> set;
    private String ultimoRegistroIps;
    private String estado;
    private String mensaje;
    private Ips ips;*/