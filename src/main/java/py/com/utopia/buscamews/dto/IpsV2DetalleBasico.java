/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.utopia.buscamews.dto;

/**
 *
 * @author sergi
 */
public class IpsV2DetalleBasico {
  private String    cedula;
private String nombre;
private String sexo;
private String tipoAsegurado;
private String estado;
private String enrolado;
private String actualizado;   

    /**
     * @return the cedula
     */
    public String getCedula() {
        return cedula;
    }

    /**
     * @param cedula the cedula to set
     */
    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    /**
     * @return the nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * @param nombre the nombre to set
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * @return the sexo
     */
    public String getSexo() {
        return sexo;
    }

    /**
     * @param sexo the sexo to set
     */
    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    /**
     * @return the tipo_asegurado
     */
  

    /**
     * @return the estado
     */
    public String getEstado() {
        return estado;
    }

    /**
     * @param estado the estado to set
     */
    public void setEstado(String estado) {
        this.estado = estado;
    }

    /**
     * @return the enrolado
     */
    public String getEnrolado() {
        return enrolado;
    }

    /**
     * @param enrolado the enrolado to set
     */
    public void setEnrolado(String enrolado) {
        this.enrolado = enrolado;
    }

    /**
     * @return the actualizado
     */
    public String getActualizado() {
        return actualizado;
    }

    /**
     * @param actualizado the actualizado to set
     */
    public void setActualizado(String actualizado) {
        this.actualizado = actualizado;
    }

    /**
     * @return the tipoAsegurado
     */
    public String getTipoAsegurado() {
        return tipoAsegurado;
    }

    /**
     * @param tipoAsegurado the tipoAsegurado to set
     */
    public void setTipoAsegurado(String tipoAsegurado) {
        this.tipoAsegurado = tipoAsegurado;
    }
}
