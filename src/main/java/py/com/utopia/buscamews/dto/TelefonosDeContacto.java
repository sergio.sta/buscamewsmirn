/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.utopia.buscamews.dto;

/**
 *
 * @author sergio
 * @author Fernando Invernizzi <ferinver92@gmail.com>
 */
public class TelefonosDeContacto {

    private String operador;
    private String nroCuenta;
    private String billing;
    private String nombres;
    private String apellidos;
    private String fechaActivacion;

    public TelefonosDeContacto() {
    }

    public String getOperador() {
        return operador;
    }

    public void setOperador(String operador) {
        this.operador = operador;
    }

    public String getNroCuenta() {
        return nroCuenta;
    }

    public void setNroCuenta(String nroCuenta) {
        this.nroCuenta = nroCuenta;
    }

    public String getBilling() {
        return billing;
    }

    public void setBilling(String billing) {
        this.billing = billing;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getFechaActivacion() {
        return fechaActivacion;
    }

    public void setFechaActivacion(String fechaActivacion) {
        this.fechaActivacion = fechaActivacion;
    }

    @Override
    public String toString() {
        return "TelefonosContactos{" + "operador=" + operador + ", nroCuenta=" + nroCuenta + ", billing=" + billing + ", nombres=" + nombres + ", apellidos=" + apellidos + ", fechaActivacion=" + fechaActivacion + '}';
    }

}
