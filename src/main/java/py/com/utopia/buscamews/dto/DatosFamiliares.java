/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.utopia.buscamews.dto;

/**
 *
 * @author sergio
 * @author Fernando Invernizzi <ferinver92@gmail.com>
 */
public class DatosFamiliares {

    String nroDocPadre;
    String nombrePadre;
    String apellidoPadre;
    String nroDocMadre;
    String nombreMadre;
    String apellidoMadre;
    String nroDocConyuge;
    String nombreConyuge;
    String apellidoConyuge;

    public DatosFamiliares() {
    }

    public String getNroDocPadre() {
        return nroDocPadre;
    }

    public void setNroDocPadre(String nroDocPadre) {
        this.nroDocPadre = nroDocPadre;
    }

    public String getNombrePadre() {
        return nombrePadre;
    }

    public void setNombrePadre(String nombrePadre) {
        this.nombrePadre = nombrePadre;
    }

    public String getApellidoPadre() {
        return apellidoPadre;
    }

    public void setApellidoPadre(String apellidoPadre) {
        this.apellidoPadre = apellidoPadre;
    }

    public String getNroDocMadre() {
        return nroDocMadre;
    }

    public void setNroDocMadre(String nroDocMadre) {
        this.nroDocMadre = nroDocMadre;
    }

    public String getNombreMadre() {
        return nombreMadre;
    }

    public void setNombreMadre(String nombreMadre) {
        this.nombreMadre = nombreMadre;
    }

    public String getApellidoMadre() {
        return apellidoMadre;
    }

    public void setApellidoMadre(String apellidoMadre) {
        this.apellidoMadre = apellidoMadre;
    }

    public String getNroDocConyuge() {
        return nroDocConyuge;
    }

    public void setNroDocConyuge(String nroDocConyuge) {
        this.nroDocConyuge = nroDocConyuge;
    }

    public String getNombreConyuge() {
        return nombreConyuge;
    }

    public void setNombreConyuge(String nombreConyuge) {
        this.nombreConyuge = nombreConyuge;
    }

    public String getApellidoConyuge() {
        return apellidoConyuge;
    }

    public void setApellidoConyuge(String apellidoConyuge) {
        this.apellidoConyuge = apellidoConyuge;
    }

    @Override
    public String toString() {
        return "DatosFamiliares{" + "nroDocPadre=" + nroDocPadre + ", nombrePadre=" + nombrePadre + ", apellidoPadre=" + apellidoPadre + ", nroDocMadre=" + nroDocMadre + ", nombreMadre=" + nombreMadre + ", apellidoMadre=" + apellidoMadre + ", nroDocConyuge=" + nroDocConyuge + ", nombreConyuge=" + nombreConyuge + ", apellidoConyuge=" + apellidoConyuge + '}';
    }

}
