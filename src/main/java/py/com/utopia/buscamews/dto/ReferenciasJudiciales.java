/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.utopia.buscamews.dto;

/**
 *
 * @author sergi
 */
public class ReferenciasJudiciales {
private String    proceso;
private String causa;
private String fechaSello;
private String caratula;
private String juzgado;
private String secretaria;
private String demandante;
private String ultimaActuacionFecha;
private String ultimaActuacion;
private String estado;

    /**
     * @return the proceso
     */
    public String getProceso() {
        return proceso;
    }

    /**
     * @param proceso the proceso to set
     */
    public void setProceso(String proceso) {
        this.proceso = proceso;
    }

    /**
     * @return the causa
     */
    public String getCausa() {
        return causa;
    }

    /**
     * @param causa the causa to set
     */
    public void setCausa(String causa) {
        this.causa = causa;
    }

    /**
     * @return the fechaSello
     */
    public String getFechaSello() {
        return fechaSello;
    }

    /**
     * @param fechaSello the fechaSello to set
     */
    public void setFechaSello(String fechaSello) {
        this.fechaSello = fechaSello;
    }

    /**
     * @return the caratula
     */
    public String getCaratula() {
        return caratula;
    }

    /**
     * @param caratula the caratula to set
     */
    public void setCaratula(String caratula) {
        this.caratula = caratula;
    }

    /**
     * @return the juzgado
     */
    public String getJuzgado() {
        return juzgado;
    }

    /**
     * @param juzgado the juzgado to set
     */
    public void setJuzgado(String juzgado) {
        this.juzgado = juzgado;
    }

    /**
     * @return the secretaria
     */
    public String getSecretaria() {
        return secretaria;
    }

    /**
     * @param secretaria the secretaria to set
     */
    public void setSecretaria(String secretaria) {
        this.secretaria = secretaria;
    }

    /**
     * @return the demandante
     */
    public String getDemandante() {
        return demandante;
    }

    /**
     * @param demandante the demandante to set
     */
    public void setDemandante(String demandante) {
        this.demandante = demandante;
    }

    /**
     * @return the ultimaActuacionFecha
     */
    public String getUltimaActuacionFecha() {
        return ultimaActuacionFecha;
    }

    /**
     * @param ultimaActuacionFecha the ultimaActuacionFecha to set
     */
    public void setUltimaActuacionFecha(String ultimaActuacionFecha) {
        this.ultimaActuacionFecha = ultimaActuacionFecha;
    }

    /**
     * @return the ultimaActuacion
     */
    public String getUltimaActuacion() {
        return ultimaActuacion;
    }

    /**
     * @param ultimaActuacion the ultimaActuacion to set
     */
    public void setUltimaActuacion(String ultimaActuacion) {
        this.ultimaActuacion = ultimaActuacion;
    }

    /**
     * @return the estado
     */
    public String getEstado() {
        return estado;
    }

    /**
     * @param estado the estado to set
     */
    public void setEstado(String estado) {
        this.estado = estado;
    }

    public ReferenciasJudiciales() {
    }

  
}
