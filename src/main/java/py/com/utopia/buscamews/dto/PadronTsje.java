/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.utopia.buscamews.dto;

/**
 *
 * @author sergio
 * @author Fernando Invernizzi <ferinver92@gmail.com>
 */
public class PadronTsje {

    private String departamento;
    private String distrito;
    private String zona;
    private String local;
    private String fechaInscripcion;

    public PadronTsje() {
    }

    public String getDepartamento() {
        return departamento;
    }

    public void setDepartamento(String departamento) {
        this.departamento = departamento;
    }

    public String getDistrito() {
        return distrito;
    }

    public void setDistrito(String distrito) {
        this.distrito = distrito;
    }

    public String getZona() {
        return zona;
    }

    public void setZona(String zona) {
        this.zona = zona;
    }

    public String getLocal() {
        return local;
    }

    public void setLocal(String local) {
        this.local = local;
    }

    public String getFechaInscripcion() {
        return fechaInscripcion;
    }

    public void setFechaInscripcion(String fechaInscripcion) {
        this.fechaInscripcion = fechaInscripcion;
    }

    @Override
    public String toString() {
        return "PadronTsje{" + "departamento=" + departamento + ", distrito=" + distrito + ", zona=" + zona + ", local=" + local + ", fechaInscripcion=" + fechaInscripcion + '}';
    }

}
