/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.utopia.buscamews.dto;

/**
 *
 * @author sergi
 */
public class IpsV2DetallePatronal {
//  private String   tipo;
//private String cedula;
//private String nombre;
//private String fechaNac;
//private String edad;
//private String estado;
    
    
    private String tipoSeguro;
private String estado;
private String mesesAporte;
private String entrada;
private String salida;
private String actualizado;
private String empleador;



    /**
     * @return the estado
     */
    public String getEstado() {
        return estado;
    }

    /**
     * @param estado the estado to set
     */
    public void setEstado(String estado) {
        this.estado = estado;
    }

    /**
     * @return the mesesAporte
     */
    public String getMesesAporte() {
        return mesesAporte;
    }

    /**
     * @param mesesAporte the mesesAporte to set
     */
    public void setMesesAporte(String mesesAporte) {
        this.mesesAporte = mesesAporte;
    }

    /**
     * @return the entrada
     */
    public String getEntrada() {
        return entrada;
    }

    /**
     * @param entrada the entrada to set
     */
    public void setEntrada(String entrada) {
        this.entrada = entrada;
    }

    /**
     * @return the salida
     */
    public String getSalida() {
        return salida;
    }

    /**
     * @param salida the salida to set
     */
    public void setSalida(String salida) {
        this.salida = salida;
    }

    /**
     * @return the actualizado
     */
    public String getActualizado() {
        return actualizado;
    }

    /**
     * @param actualizado the actualizado to set
     */
    public void setActualizado(String actualizado) {
        this.actualizado = actualizado;
    }

    /**
     * @return the empleador
     */
    public String getEmpleador() {
        return empleador;
    }

    /**
     * @param empleador the empleador to set
     */
    public void setEmpleador(String empleador) {
        this.empleador = empleador;
    }

    /**
     * @return the tipoSeguro
     */
    public String getTipoSeguro() {
        return tipoSeguro;
    }

    /**
     * @param tipoSeguro the tipoSeguro to set
     */
    public void setTipoSeguro(String tipoSeguro) {
        this.tipoSeguro = tipoSeguro;
    }

   
}
