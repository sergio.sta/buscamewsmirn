/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.utopia.buscamews.dto;

/**
 *
 * @author sergi
 */
public class RegistrosAbogados {
    
private String   profesion;
private String   nroMatricula;
private String   fechaExpMatricula;
private String   habilitacion;
private String   nombre;
private String   nroContacto1;
private String   nroContacto2;
private String   direccionParticular;
private String   fechaActualizacion;

    public RegistrosAbogados() {
    }

    /**
     * @return the profesion
     */
    public String getProfesion() {
        return profesion;
    }

    /**
     * @param profesion the profesion to set
     */
    public void setProfesion(String profesion) {
        this.profesion = profesion;
    }

    /**
     * @return the nroMatricula
     */
    public String getNroMatricula() {
        return nroMatricula;
    }

    /**
     * @param nroMatricula the nroMatricula to set
     */
    public void setNroMatricula(String nroMatricula) {
        this.nroMatricula = nroMatricula;
    }

    /**
     * @return the fechaExpMatricula
     */
    public String getFechaExpMatricula() {
        return fechaExpMatricula;
    }

    /**
     * @param fechaExpMatricula the fechaExpMatricula to set
     */
    public void setFechaExpMatricula(String fechaExpMatricula) {
        this.fechaExpMatricula = fechaExpMatricula;
    }

    /**
     * @return the habilitacion
     */
    public String getHabilitacion() {
        return habilitacion;
    }

    /**
     * @param habilitacion the habilitacion to set
     */
    public void setHabilitacion(String habilitacion) {
        this.habilitacion = habilitacion;
    }

    /**
     * @return the nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * @param nombre the nombre to set
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * @return the nroContacto1
     */
    public String getNroContacto1() {
        return nroContacto1;
    }

    /**
     * @param nroContacto1 the nroContacto1 to set
     */
    public void setNroContacto1(String nroContacto1) {
        this.nroContacto1 = nroContacto1;
    }

    /**
     * @return the nroContacto2
     */
    public String getNroContacto2() {
        return nroContacto2;
    }

    /**
     * @param nroContacto2 the nroContacto2 to set
     */
    public void setNroContacto2(String nroContacto2) {
        this.nroContacto2 = nroContacto2;
    }

    /**
     * @return the direccion_particular
     */
   

    /**
     * @return the fechaActualizacion
     */
    public String getFechaActualizacion() {
        return fechaActualizacion;
    }

    /**
     * @param fechaActualizacion the fechaActualizacion to set
     */
    public void setFechaActualizacion(String fechaActualizacion) {
        this.fechaActualizacion = fechaActualizacion;
    }

    /**
     * @return the direccionParticular
     */
    public String getDireccionParticular() {
        return direccionParticular;
    }

    /**
     * @param direccionParticular the direccionParticular to set
     */
    public void setDireccionParticular(String direccionParticular) {
        this.direccionParticular = direccionParticular;
    }

}
