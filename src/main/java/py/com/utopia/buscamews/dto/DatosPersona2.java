/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.utopia.buscamews.dto;

import java.util.List;

/**
 *
 * @author sergio
 * @author Fernando Invernizzi <ferinver92@gmail.com>
 */
public class DatosPersona2 {
  
    private String nroDocumento;
    private String tipo;
    private String nombre;
    private String apellidos;
    private String fechaNac;
    private String edad;
    private String nacionalidad;
    private String nacionalidadOrigen;
    private String lugar;
    private String profesion;
    private String sexo;
    private String estadoCiv;
    private String domicilio;
    private String barrio;
    private String ciudad;
    private String fechaEmisionCi;
    private String fechaGrabCi;
    private String fechaVencCi;
    private String fechaAproxVto;
    private String nroDocExtranjero;
    private String telefono;
    private DatosFamiliares familiares;
    private List<PosiblesHermanos> hermanos;
    private List<PadronTsje> padron;
    private List<TelefonosDeContacto> telefonos;
    private List<Set> set;
    private String ultimoRegistroIps;
    private String estado;
    private String mensaje;
    private List<Ips> ips;
    private List<ListaNegra> listaNegra;
    private List<AntecedentesPolicial> antecedentesPolicial;
    private List<BNFSalario> bnfSalario;
    private List<ReferenciasJudiciales> referenciasJudiciales;
private List<DisolucionConyugal> disolucionConyugal ;
private List<OperacionesBancarias> operacionesBancarias;
private List<RegistrosAbogados> registroAbogados;
private List<PPRHermano> pprHermano;
private List<Fallecido> fallecido;
private List<HistorialConsulta> historialConsulta;
private List<PPRHijos> pprHijos;
private String pdf_url;
    public DatosPersona2() {
    }

    /**
     * @return the nroDocumento
     */
    public String getNroDocumento() {
        return nroDocumento;
    }

    /**
     * @param nroDocumento the nroDocumento to set
     */
    public void setNroDocumento(String nroDocumento) {
        this.nroDocumento = nroDocumento;
    }

    /**
     * @return the tipo
     */
    public String getTipo() {
        return tipo;
    }

    /**
     * @param tipo the tipo to set
     */
    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    /**
     * @return the nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * @param nombre the nombre to set
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * @return the apellidos
     */
    public String getApellidos() {
        return apellidos;
    }

    /**
     * @param apellidos the apellidos to set
     */
    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    /**
     * @return the fechaNac
     */
    public String getFechaNac() {
        return fechaNac;
    }

    /**
     * @param fechaNac the fechaNac to set
     */
    public void setFechaNac(String fechaNac) {
        this.fechaNac = fechaNac;
    }

    /**
     * @return the edad
     */
    public String getEdad() {
        return edad;
    }

    /**
     * @param edad the edad to set
     */
    public void setEdad(String edad) {
        this.edad = edad;
    }

    /**
     * @return the nacionalidad
     */
    public String getNacionalidad() {
        return nacionalidad;
    }

    /**
     * @param nacionalidad the nacionalidad to set
     */
    public void setNacionalidad(String nacionalidad) {
        this.nacionalidad = nacionalidad;
    }

    /**
     * @return the nacionalidadOrigen
     */
    public String getNacionalidadOrigen() {
        return nacionalidadOrigen;
    }

    /**
     * @param nacionalidadOrigen the nacionalidadOrigen to set
     */
    public void setNacionalidadOrigen(String nacionalidadOrigen) {
        this.nacionalidadOrigen = nacionalidadOrigen;
    }

    /**
     * @return the lugar
     */
    public String getLugar() {
        return lugar;
    }

    /**
     * @param lugar the lugar to set
     */
    public void setLugar(String lugar) {
        this.lugar = lugar;
    }

    /**
     * @return the profesion
     */
    public String getProfesion() {
        return profesion;
    }

    /**
     * @param profesion the profesion to set
     */
    public void setProfesion(String profesion) {
        this.profesion = profesion;
    }

    /**
     * @return the sexo
     */
    public String getSexo() {
        return sexo;
    }

    /**
     * @param sexo the sexo to set
     */
    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    /**
     * @return the estadoCiv
     */
    public String getEstadoCiv() {
        return estadoCiv;
    }

    /**
     * @param estadoCiv the estadoCiv to set
     */
    public void setEstadoCiv(String estadoCiv) {
        this.estadoCiv = estadoCiv;
    }

    /**
     * @return the domicilio
     */
    public String getDomicilio() {
        return domicilio;
    }

    /**
     * @param domicilio the domicilio to set
     */
    public void setDomicilio(String domicilio) {
        this.domicilio = domicilio;
    }

    /**
     * @return the barrio
     */
    public String getBarrio() {
        return barrio;
    }

    /**
     * @param barrio the barrio to set
     */
    public void setBarrio(String barrio) {
        this.barrio = barrio;
    }

    /**
     * @return the ciudad
     */
    public String getCiudad() {
        return ciudad;
    }

    /**
     * @param ciudad the ciudad to set
     */
    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    /**
     * @return the fechaEmisionCi
     */
    public String getFechaEmisionCi() {
        return fechaEmisionCi;
    }

    /**
     * @param fechaEmisionCi the fechaEmisionCi to set
     */
    public void setFechaEmisionCi(String fechaEmisionCi) {
        this.fechaEmisionCi = fechaEmisionCi;
    }

    /**
     * @return the fechaGrabCi
     */
    public String getFechaGrabCi() {
        return fechaGrabCi;
    }

    /**
     * @param fechaGrabCi the fechaGrabCi to set
     */
    public void setFechaGrabCi(String fechaGrabCi) {
        this.fechaGrabCi = fechaGrabCi;
    }

    /**
     * @return the fechaVencCi
     */
    public String getFechaVencCi() {
        return fechaVencCi;
    }

    /**
     * @param fechaVencCi the fechaVencCi to set
     */
    public void setFechaVencCi(String fechaVencCi) {
        this.fechaVencCi = fechaVencCi;
    }

    /**
     * @return the fechaAproxVto
     */
    public String getFechaAproxVto() {
        return fechaAproxVto;
    }

    /**
     * @param fechaAproxVto the fechaAproxVto to set
     */
    public void setFechaAproxVto(String fechaAproxVto) {
        this.fechaAproxVto = fechaAproxVto;
    }

    /**
     * @return the nroDocExtranjero
     */
    public String getNroDocExtranjero() {
        return nroDocExtranjero;
    }

    /**
     * @param nroDocExtranjero the nroDocExtranjero to set
     */
    public void setNroDocExtranjero(String nroDocExtranjero) {
        this.nroDocExtranjero = nroDocExtranjero;
    }

    /**
     * @return the telefono
     */
    public String getTelefono() {
        return telefono;
    }

    /**
     * @param telefono the telefono to set
     */
    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    /**
     * @return the familiares
     */
    public DatosFamiliares getFamiliares() {
        return familiares;
    }

    /**
     * @param familiares the familiares to set
     */
    public void setFamiliares(DatosFamiliares familiares) {
        this.familiares = familiares;
    }

    /**
     * @return the hermanos
     */
    public List<PosiblesHermanos> getHermanos() {
        return hermanos;
    }

    /**
     * @param hermanos the hermanos to set
     */
    public void setHermanos(List<PosiblesHermanos> hermanos) {
        this.hermanos = hermanos;
    }

    /**
     * @return the padron
     */
    public List<PadronTsje> getPadron() {
        return padron;
    }

    /**
     * @param padron the padron to set
     */
    public void setPadron(List<PadronTsje> padron) {
        this.padron = padron;
    }

    /**
     * @return the telefonos
     */
    public List<TelefonosDeContacto> getTelefonos() {
        return telefonos;
    }

    /**
     * @param telefonos the telefonos to set
     */
    public void setTelefonos(List<TelefonosDeContacto> telefonos) {
        this.telefonos = telefonos;
    }

    /**
     * @return the set
     */
    public List<Set> getSet() {
        return set;
    }

    /**
     * @param set the set to set
     */
    public void setSet(List<Set> set) {
        this.set = set;
    }

    /**
     * @return the ultimoRegistroIps
     */
    public String getUltimoRegistroIps() {
        return ultimoRegistroIps;
    }

    /**
     * @param ultimoRegistroIps the ultimoRegistroIps to set
     */
    public void setUltimoRegistroIps(String ultimoRegistroIps) {
        this.ultimoRegistroIps = ultimoRegistroIps;
    }

    /**
     * @return the estado
     */
    public String getEstado() {
        return estado;
    }

    /**
     * @param estado the estado to set
     */
    public void setEstado(String estado) {
        this.estado = estado;
    }

    /**
     * @return the mensaje
     */
    public String getMensaje() {
        return mensaje;
    }

    /**
     * @param mensaje the mensaje to set
     */
    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    /**
     * @return the ips
     */
    public List<Ips> getIps() {
        return ips;
    }

    /**
     * @param ips the ips to set
     */
    public void setIps(List<Ips> ips) {
        this.ips = ips;
    }

    

    /**
     * @return the antecedentesPolicial
     */
    public List<AntecedentesPolicial> getAntecedentesPolicial() {
        return antecedentesPolicial;
    }

    /**
     * @param antecedentesPolicial the antecedentesPolicial to set
     */
    public void setAntecedentesPolicial(List<AntecedentesPolicial> antecedentesPolicial) {
        this.antecedentesPolicial = antecedentesPolicial;
    }

    /**
     * @return the bnfSalario
     */
    public List<BNFSalario> getBnfSalario() {
        return bnfSalario;
    }

    /**
     * @param bnfSalario the bnfSalario to set
     */
    public void setBnfSalario(List<BNFSalario> bnfSalario) {
        this.bnfSalario = bnfSalario;
    }

    /**
     * @return the referenciasJudiciales
     */
    public List<ReferenciasJudiciales> getReferenciasJudiciales() {
        return referenciasJudiciales;
    }

    /**
     * @param referenciasJudiciales the referenciasJudiciales to set
     */
    public void setReferenciasJudiciales(List<ReferenciasJudiciales> referenciasJudiciales) {
        this.referenciasJudiciales = referenciasJudiciales;
    }

    /**
     * @return the disolucionConyugal
     */
    public List<DisolucionConyugal> getDisolucionConyugal() {
        return disolucionConyugal;
    }

    /**
     * @param disolucionConyugal the disolucionConyugal to set
     */
    public void setDisolucionConyugal(List<DisolucionConyugal> disolucionConyugal) {
        this.disolucionConyugal = disolucionConyugal;
    }

    /**
     * @return the operacionesBancarias
     */
    public List<OperacionesBancarias> getOperacionesBancarias() {
        return operacionesBancarias;
    }

    /**
     * @param operacionesBancarias the operacionesBancarias to set
     */
    public void setOperacionesBancarias(List<OperacionesBancarias> operacionesBancarias) {
        this.operacionesBancarias = operacionesBancarias;
    }

    /**
     * @return the registroAbogados
     */
    public List<RegistrosAbogados> getRegistroAbogados() {
        return registroAbogados;
    }

    /**
     * @param registroAbogados the registroAbogados to set
     */
    public void setRegistroAbogados(List<RegistrosAbogados> registroAbogados) {
        this.registroAbogados = registroAbogados;
    }

    /**
     * @return the pprHermano
     */
    public List<PPRHermano> getPprHermano() {
        return pprHermano;
    }

    /**
     * @param pprHermano the pprHermano to set
     */
    public void setPprHermano(List<PPRHermano> pprHermano) {
        this.pprHermano = pprHermano;
    }

    /**
     * @return the fallecido
     */
    public List<Fallecido> getFallecido() {
        return fallecido;
    }

    /**
     * @param fallecido the fallecido to set
     */
    public void setFallecido(List<Fallecido> fallecido) {
        this.fallecido = fallecido;
    }

    /**
     * @return the listaNegra
     */
    public List<ListaNegra> getListaNegra() {
        return listaNegra;
    }

    /**
     * @param listaNegra the listaNegra to set
     */
    public void setListaNegra(List<ListaNegra> listaNegra) {
        this.listaNegra = listaNegra;
    }

    /**
     * @return the historialConsulta
     */
    public List<HistorialConsulta> getHistorialConsulta() {
        return historialConsulta;
    }

    /**
     * @param historialConsulta the historialConsulta to set
     */
    public void setHistorialConsulta(List<HistorialConsulta> historialConsulta) {
        this.historialConsulta = historialConsulta;
    }

    /**
     * @return the pprHijos
     */
    public List<PPRHijos> getPprHijos() {
        return pprHijos;
    }

    /**
     * @param pprHijos the pprHijos to set
     */
    public void setPprHijos(List<PPRHijos> pprHijos) {
        this.pprHijos = pprHijos;
    }

    /**
     * @return the pdf_url
     */
    public String getPdf_url() {
        return pdf_url;
    }

    /**
     * @param pdf_url the pdf_url to set
     */
    public void setPdf_url(String pdf_url) {
        this.pdf_url = pdf_url;
    }

   

}
