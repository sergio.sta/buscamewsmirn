/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.utopia.buscamews.dto;

/**
 *
 * @author sergi
 */
public class DisolucionConyugal {
private String   fechaPublicacion;
private String   esposaCedula;
private String   esposaNombre;
private String   esposoCedula;
private String   esposoNombre;
private String   juzgado;
private String   juez;
private String   secretaria;
private String   diario;
private String   caratula;

    /**
     * @return the fechaPublicacion
     */
    public String getFechaPublicacion() {
        return fechaPublicacion;
    }

    /**
     * @param fechaPublicacion the fechaPublicacion to set
     */
    public void setFechaPublicacion(String fechaPublicacion) {
        this.fechaPublicacion = fechaPublicacion;
    }

    /**
     * @return the esposaCedula
     */
    public String getEsposaCedula() {
        return esposaCedula;
    }

    /**
     * @param esposaCedula the esposaCedula to set
     */
    public void setEsposaCedula(String esposaCedula) {
        this.esposaCedula = esposaCedula;
    }

    /**
     * @return the esposaNombre
     */
    public String getEsposaNombre() {
        return esposaNombre;
    }

    /**
     * @param esposaNombre the esposaNombre to set
     */
    public void setEsposaNombre(String esposaNombre) {
        this.esposaNombre = esposaNombre;
    }

    /**
     * @return the esposoCedula
     */
    public String getEsposoCedula() {
        return esposoCedula;
    }

    /**
     * @param esposoCedula the esposoCedula to set
     */
    public void setEsposoCedula(String esposoCedula) {
        this.esposoCedula = esposoCedula;
    }

    /**
     * @return the esposoNombre
     */
    public String getEsposoNombre() {
        return esposoNombre;
    }

    /**
     * @param esposoNombre the esposoNombre to set
     */
    public void setEsposoNombre(String esposoNombre) {
        this.esposoNombre = esposoNombre;
    }

    /**
     * @return the juzgado
     */
    public String getJuzgado() {
        return juzgado;
    }

    /**
     * @param juzgado the juzgado to set
     */
    public void setJuzgado(String juzgado) {
        this.juzgado = juzgado;
    }

    /**
     * @return the juez
     */
    public String getJuez() {
        return juez;
    }

    /**
     * @param juez the juez to set
     */
    public void setJuez(String juez) {
        this.juez = juez;
    }

    /**
     * @return the secretaria
     */
    public String getSecretaria() {
        return secretaria;
    }

    /**
     * @param secretaria the secretaria to set
     */
    public void setSecretaria(String secretaria) {
        this.secretaria = secretaria;
    }

    /**
     * @return the diario
     */
    public String getDiario() {
        return diario;
    }

    /**
     * @param diario the diario to set
     */
    public void setDiario(String diario) {
        this.diario = diario;
    }

    /**
     * @return the caratula
     */
    public String getCaratula() {
        return caratula;
    }

    /**
     * @param caratula the caratula to set
     */
    public void setCaratula(String caratula) {
        this.caratula = caratula;
    }

   
}
