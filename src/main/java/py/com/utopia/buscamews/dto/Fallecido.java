/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.utopia.buscamews.dto;

/**
 *
 * @author sergi
 */
public class Fallecido {
 private String   cedula;
private String  nombres;
private String  apellidos;
private String  edad;
private String  fechaDefuncion;
private String  anho;
private String  origenDatos;
private String  verificado;
private String  codiLibro;
private String  tomo;
private String  acta;
private String  folio;

    /**
     * @return the cedula
     */
    public String getCedula() {
        return cedula;
    }

    /**
     * @param cedula the cedula to set
     */
    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    /**
     * @return the nombres
     */
    public String getNombres() {
        return nombres;
    }

    /**
     * @param nombres the nombres to set
     */
    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    /**
     * @return the apellidos
     */
    public String getApellidos() {
        return apellidos;
    }

    /**
     * @param apellidos the apellidos to set
     */
    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    /**
     * @return the edad
     */
    public String getEdad() {
        return edad;
    }

    /**
     * @param edad the edad to set
     */
    public void setEdad(String edad) {
        this.edad = edad;
    }

    /**
     * @return the fechaDefuncion
     */
    public String getFechaDefuncion() {
        return fechaDefuncion;
    }

    /**
     * @param fechaDefuncion the fechaDefuncion to set
     */
    public void setFechaDefuncion(String fechaDefuncion) {
        this.fechaDefuncion = fechaDefuncion;
    }

    /**
     * @return the anho
     */
    public String getAnho() {
        return anho;
    }

    /**
     * @param anho the anho to set
     */
    public void setAnho(String anho) {
        this.anho = anho;
    }

    /**
     * @return the origenDatos
     */
    public String getOrigenDatos() {
        return origenDatos;
    }

    /**
     * @param origenDatos the origenDatos to set
     */
    public void setOrigenDatos(String origenDatos) {
        this.origenDatos = origenDatos;
    }

    /**
     * @return the verificado
     */
    public String getVerificado() {
        return verificado;
    }

    /**
     * @param verificado the verificado to set
     */
    public void setVerificado(String verificado) {
        this.verificado = verificado;
    }

    /**
     * @return the codiLibro
     */
    public String getCodiLibro() {
        return codiLibro;
    }

    /**
     * @param codiLibro the codiLibro to set
     */
    public void setCodiLibro(String codiLibro) {
        this.codiLibro = codiLibro;
    }

    /**
     * @return the tomo
     */
    public String getTomo() {
        return tomo;
    }

    /**
     * @param tomo the tomo to set
     */
    public void setTomo(String tomo) {
        this.tomo = tomo;
    }

    /**
     * @return the acta
     */
    public String getActa() {
        return acta;
    }

    /**
     * @param acta the acta to set
     */
    public void setActa(String acta) {
        this.acta = acta;
    }

    /**
     * @return the folio
     */
    public String getFolio() {
        return folio;
    }

    /**
     * @param folio the folio to set
     */
    public void setFolio(String folio) {
        this.folio = folio;
    }
}
