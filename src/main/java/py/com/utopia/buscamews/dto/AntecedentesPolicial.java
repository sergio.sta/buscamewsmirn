/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.utopia.buscamews.dto;

/**
 *
 * @author sergi
 */
public class AntecedentesPolicial {
private String  cedula;
private String lugar;
private String causa;
private String nroResolucion;
private String fechaResolucion;
private String fechaOficio;

    /**
     * @return the cedula
     */
    public String getCedula() {
        return cedula;
    }

    /**
     * @param cedula the cedula to set
     */
    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    /**
     * @return the lugar
     */
    public String getLugar() {
        return lugar;
    }

    /**
     * @param lugar the lugar to set
     */
    public void setLugar(String lugar) {
        this.lugar = lugar;
    }

    /**
     * @return the causa
     */
    public String getCausa() {
        return causa;
    }

    /**
     * @param causa the causa to set
     */
    public void setCausa(String causa) {
        this.causa = causa;
    }

    /**
     * @return the nroResolucion
     */
    public String getNroResolucion() {
        return nroResolucion;
    }

    /**
     * @param nroResolucion the nroResolucion to set
     */
    public void setNroResolucion(String nroResolucion) {
        this.nroResolucion = nroResolucion;
    }

    /**
     * @return the fechaResolucion
     */
    public String getFechaResolucion() {
        return fechaResolucion;
    }

    /**
     * @param fechaResolucion the fechaResolucion to set
     */
    public void setFechaResolucion(String fechaResolucion) {
        this.fechaResolucion = fechaResolucion;
    }

    /**
     * @return the fechaOficio
     */
    public String getFechaOficio() {
        return fechaOficio;
    }

    /**
     * @param fechaOficio the fechaOficio to set
     */
    public void setFechaOficio(String fechaOficio) {
        this.fechaOficio = fechaOficio;
    }

   
}
