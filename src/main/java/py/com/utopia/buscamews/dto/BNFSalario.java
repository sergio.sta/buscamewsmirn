/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.utopia.buscamews.dto;

/**
 *
 * @author sergi
 */
public class BNFSalario {
 private String    cedula;
private String nombre;
private String anho;
private String mes;
private String liquido;
private String sobreGiro;
private String prestamo;
private String tarjeta;
private String otros;
private String neto;

    /**
     * @return the cedula
     */
    public String getCedula() {
        return cedula;
    }

    /**
     * @param cedula the cedula to set
     */
    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    /**
     * @return the nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * @param nombre the nombre to set
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * @return the anho
     */
    public String getAnho() {
        return anho;
    }

    /**
     * @param anho the anho to set
     */
    public void setAnho(String anho) {
        this.anho = anho;
    }

    /**
     * @return the mes
     */
    public String getMes() {
        return mes;
    }

    /**
     * @param mes the mes to set
     */
    public void setMes(String mes) {
        this.mes = mes;
    }

    /**
     * @return the liquido
     */
    public String getLiquido() {
        return liquido;
    }

    /**
     * @param liquido the liquido to set
     */
    public void setLiquido(String liquido) {
        this.liquido = liquido;
    }

   

    /**
     * @return the prestamo
     */
    public String getPrestamo() {
        return prestamo;
    }

    /**
     * @param prestamo the prestamo to set
     */
    public void setPrestamo(String prestamo) {
        this.prestamo = prestamo;
    }

    /**
     * @return the tarjeta
     */
    public String getTarjeta() {
        return tarjeta;
    }

    /**
     * @param tarjeta the tarjeta to set
     */
    public void setTarjeta(String tarjeta) {
        this.tarjeta = tarjeta;
    }

    /**
     * @return the otros
     */
    public String getOtros() {
        return otros;
    }

    /**
     * @param otros the otros to set
     */
    public void setOtros(String otros) {
        this.otros = otros;
    }

    /**
     * @return the neto
     */
    public String getNeto() {
        return neto;
    }

    /**
     * @param neto the neto to set
     */
    public void setNeto(String neto) {
        this.neto = neto;
    }

    /**
     * @return the sobreGiro
     */
    public String getSobreGiro() {
        return sobreGiro;
    }

    /**
     * @param sobreGiro the sobreGiro to set
     */
    public void setSobreGiro(String sobreGiro) {
        this.sobreGiro = sobreGiro;
    }
}
