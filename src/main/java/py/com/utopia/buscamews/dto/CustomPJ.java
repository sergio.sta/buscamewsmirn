/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.utopia.buscamews.dto;

/**
 *
 * @author sergi
 */
public class CustomPJ {
    private String consultadaXCobranzaUltimoAnno;
    private String cantidadConsultasUltimos60Dias;

    public String getConsultadaXCobranzaUltimoAnno() {
        return consultadaXCobranzaUltimoAnno;
    }

    public void setConsultadaXCobranzaUltimoAnno(String consultadaXCobranzaUltimoAnno) {
        this.consultadaXCobranzaUltimoAnno = consultadaXCobranzaUltimoAnno;
    }

    public String getCantidadConsultasUltimos60Dias() {
        return cantidadConsultasUltimos60Dias;
    }

    public void setCantidadConsultasUltimos60Dias(String cantidadConsultasUltimos60Dias) {
        this.cantidadConsultasUltimos60Dias = cantidadConsultasUltimos60Dias;
    }
    
}
