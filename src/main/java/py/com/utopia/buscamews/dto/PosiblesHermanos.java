/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.utopia.buscamews.dto;

/**
 *
 * @author sergio
 * @author Fernando Invernizzi <ferinver92@gmail.com>
 */
public class PosiblesHermanos {

    private String cedula;
    private String nombres;
    private String apellidos;
    private String lugarNac;
    private String fechaNac;
    private String edad;
    private String nacionalidad;

    public PosiblesHermanos() {
    }

    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getLugarNac() {
        return lugarNac;
    }

    public void setLugarNac(String lugarNac) {
        this.lugarNac = lugarNac;
    }

    public String getFechaNac() {
        return fechaNac;
    }

    public void setFechaNac(String fechaNac) {
        this.fechaNac = fechaNac;
    }

    public String getEdad() {
        return edad;
    }

    public void setEdad(String edad) {
        this.edad = edad;
    }

    public String getNacionalidad() {
        return nacionalidad;
    }

    public void setNacionalidad(String nacionalidad) {
        this.nacionalidad = nacionalidad;
    }

    @Override
    public String toString() {
        return "PosiblesHermanos{" + "cedula=" + cedula + ", nombres=" + nombres + ", apellidos=" + apellidos + ", lugarNac=" + lugarNac + ", fechaNac=" + fechaNac + ", edad=" + edad + ", nacionalidad=" + nacionalidad + '}';
    }

}
