/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.utopia.buscamews.dto;

/**
 *
 * @author Fernando Invernizzi <fernando.invernizzi@konecta.com.py>
 */
public class Ips {

    String cedula;
    String tipoAsegurado;
    String beneficiariosActivos;
    String nroPatronal;
    String empleador;
    String estadoEmpleador;
    String mesesAporte;
    String fechaActualizacion;
    private String nombre;
    private String ultimoPeriodo;
    private String tipoInfo;
    public Ips() {
    }

    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public String getTipoAsegurado() {
        return tipoAsegurado;
    }

    public void setTipoAsegurado(String tipoAsegurado) {
        this.tipoAsegurado = tipoAsegurado;
    }

    public String getBeneficiariosActivos() {
        return beneficiariosActivos;
    }

    public void setBeneficiariosActivos(String beneficiariosActivos) {
        this.beneficiariosActivos = beneficiariosActivos;
    }

    public String getNroPatronal() {
        return nroPatronal;
    }

    public void setNroPatronal(String nroPatronal) {
        this.nroPatronal = nroPatronal;
    }

    public String getEmpleador() {
        return empleador;
    }

    public void setEmpleador(String empleador) {
        this.empleador = empleador;
    }

    public String getEstadoEmpleador() {
        return estadoEmpleador;
    }

    public void setEstadoEmpleador(String estadoEmpleador) {
        this.estadoEmpleador = estadoEmpleador;
    }

    public String getMesesAporte() {
        return mesesAporte;
    }

    public void setMesesAporte(String mesesAporte) {
        this.mesesAporte = mesesAporte;
    }

    public String getFechaActualizacion() {
        return fechaActualizacion;
    }

    public void setFechaActualizacion(String fechaActualizacion) {
        this.fechaActualizacion = fechaActualizacion;
    }

    @Override
    public String toString() {
        return "Ips{" + "cedula=" + cedula + ", tipoAsegurado=" + tipoAsegurado + ", beneficiariosActivos=" + beneficiariosActivos + ", nroPatronal=" + nroPatronal + ", empleador=" + empleador + ", estadoEmpleador=" + estadoEmpleador + ", mesesAporte=" + mesesAporte + ", fechaActualizacion=" + fechaActualizacion + '}';
    }

    /**
     * @return the nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * @param nombre the nombre to set
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * @return the ultimoPeriodo
     */
    public String getUltimoPeriodo() {
        return ultimoPeriodo;
    }

    /**
     * @param ultimoPeriodo the ultimoPeriodo to set
     */
    public void setUltimoPeriodo(String ultimoPeriodo) {
        this.ultimoPeriodo = ultimoPeriodo;
    }

    /**
     * @return the tipoInfo
     */
    public String getTipoInfo() {
        return tipoInfo;
    }

    /**
     * @param tipoInfo the tipoInfo to set
     */
    public void setTipoInfo(String tipoInfo) {
        this.tipoInfo = tipoInfo;
    }

   
}
