/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.utopia.buscamews.dto;

/**
 *
 * @author sergi
 */
public class OperacionesBancarias {
private String     cedula;
private String  tipo;
private String  banco;
private String  anho;
private String  mes;
private String  diarioPublicado;
private String  titular;
private String  causal;

    /**
     * @return the cedula
     */
    public String getCedula() {
        return cedula;
    }

    /**
     * @param cedula the cedula to set
     */
    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    /**
     * @return the tipo
     */
    public String getTipo() {
        return tipo;
    }

    /**
     * @param tipo the tipo to set
     */
    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    /**
     * @return the banco
     */
    public String getBanco() {
        return banco;
    }

    /**
     * @param banco the banco to set
     */
    public void setBanco(String banco) {
        this.banco = banco;
    }

    /**
     * @return the anho
     */
    public String getAnho() {
        return anho;
    }

    /**
     * @param anho the anho to set
     */
    public void setAnho(String anho) {
        this.anho = anho;
    }

    /**
     * @return the mes
     */
    public String getMes() {
        return mes;
    }

    /**
     * @param mes the mes to set
     */
    public void setMes(String mes) {
        this.mes = mes;
    }

    public OperacionesBancarias() {
    }



    /**
     * @return the titular
     */
    public String getTitular() {
        return titular;
    }

    /**
     * @param titular the titular to set
     */
    public void setTitular(String titular) {
        this.titular = titular;
    }

    /**
     * @return the causal
     */
    public String getCausal() {
        return causal;
    }

    /**
     * @param causal the causal to set
     */
    public void setCausal(String causal) {
        this.causal = causal;
    }

    /**
     * @return the diarioPublicado
     */
    public String getDiarioPublicado() {
        return diarioPublicado;
    }

    /**
     * @param diarioPublicado the diarioPublicado to set
     */
    public void setDiarioPublicado(String diarioPublicado) {
        this.diarioPublicado = diarioPublicado;
    }
}
