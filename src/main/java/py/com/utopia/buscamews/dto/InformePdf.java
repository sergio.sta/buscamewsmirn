/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.utopia.buscamews.dto;

/**
 *
 * @author sergi
 */
public class InformePdf {
    private String nombrearchivo;
    private byte[] contenido;
    private String pdf_url;

    /**
     * @return the nombrearchivo
     */
    public String getNombrearchivo() {
        return nombrearchivo;
    }

    /**
     * @param nombrearchivo the nombrearchivo to set
     */
    public void setNombrearchivo(String nombrearchivo) {
        this.nombrearchivo = nombrearchivo;
    }

    /**
     * @return the contenido
     */


    /**
     * @return the pdf_url
     */
    public String getPdf_url() {
        return pdf_url;
    }

    /**
     * @param pdf_url the pdf_url to set
     */
    public void setPdf_url(String pdf_url) {
        this.pdf_url = pdf_url;
    }

    /**
     * @return the contenido
     */
    public byte[] getContenido() {
        return contenido;
    }

    /**
     * @param contenido the contenido to set
     */
    public void setContenido(byte[] contenido) {
        this.contenido = contenido;
    }

   
}
