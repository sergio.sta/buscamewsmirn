/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.utopia.buscamews;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import py.com.utopia.buscamews.dto.AntecedentesPolicial;
import py.com.utopia.buscamews.dto.BNFSalario;
import py.com.utopia.buscamews.dto.CustomPJ;
import py.com.utopia.buscamews.dto.DatosFamiliares;
import py.com.utopia.buscamews.dto.DatosPersona;
import py.com.utopia.buscamews.dto.DatosPersona2;
import py.com.utopia.buscamews.dto.DisolucionConyugal;
import py.com.utopia.buscamews.dto.Fallecido;
import py.com.utopia.buscamews.dto.HistorialConsulta;
import py.com.utopia.buscamews.dto.ImagenRostro;
import py.com.utopia.buscamews.dto.InformePdf;
import py.com.utopia.buscamews.dto.Ips;
import py.com.utopia.buscamews.dto.IpsV2DetalleBasico;
import py.com.utopia.buscamews.dto.IpsV2DetalleBeneficiario;
import py.com.utopia.buscamews.dto.IpsV2DetallePatronal;
import py.com.utopia.buscamews.dto.ListaNegra;
import py.com.utopia.buscamews.dto.OperacionesBancarias;
import py.com.utopia.buscamews.dto.PPRHermano;
import py.com.utopia.buscamews.dto.PPRHijos;
import py.com.utopia.buscamews.dto.PadronTsje;
import py.com.utopia.buscamews.dto.PosiblesHermanos;
import py.com.utopia.buscamews.dto.RedamDetalle;
import py.com.utopia.buscamews.dto.ReferenciasJudiciales;
import py.com.utopia.buscamews.dto.RegistrosAbogados;
import py.com.utopia.buscamews.dto.Set;
import py.com.utopia.buscamews.dto.TelefonosDeContacto;

/**
 *
 * @author Fernando Invernizzi <fernando.invernizzi@konecta.com.py>
 */
public class Parser {

    private final Logger logger = LogManager.getLogger(this.getClass());

    public DatosPersona2 parser2(String str){
         logger.info("IN: [" + str + "]");
        String[] values = str.split(";;");
        DatosPersona2 datosPersona = new DatosPersona2();

        for (int i = 0; i < values.length; i++) {
            String value = values[i];
            if (value.contains("--")) {
                value = value.trim();
            }
            switch (value) {
                 case "--RESULTADO--":
                    while (true) {
                        if (value.startsWith("--") && !value.equals("--RESULTADO--")) {
                            i--;
                            break;
                        } else {
                            if (!value.equals("--RESULTADO--")) {
                                String[] s = value.split("=");
                                if (s.length == 2) {
                                    String attributeName = toCamelCase(s[0]);
                                    String attributeValue =  s[1].trim();
                                    load(datosPersona, attributeName, attributeValue);
                                }
                            }
                        }
                        if (i + 1 < values.length) {
                            value = values[++i];
                        } else {
                            break;
                        }
                    }
                    break;

                case "--DATOS_PERSONA--":
                    while (true) {
                        if (value.startsWith("--") && !value.equals("--DATOS_PERSONA--")) {
                            i--;
                            break;
                        } else {
                            if (!value.equals("--DATOS_PERSONA--")) {
                                String[] s = value.split("=");
                                if (s.length == 2) {
                                    String attributeName = toCamelCase(s[0]);
                                    String attributeValue =  s[1].trim();
                                    load(datosPersona, attributeName, attributeValue);
                                }
                            }
                        }
                        if (i + 1 < values.length) {
                            value = values[++i];
                        } else {
                            break;
                        }
                    }
                    break;
                case "--DATOS_FAMILIARES--":
                    DatosFamiliares datosFamiliares = new DatosFamiliares();
                    while (true) {
                        if (value.startsWith("--") && !value.equals("--DATOS_FAMILIARES--")) {
                            datosPersona.setFamiliares(datosFamiliares);
                            i--;
                            break;
                        } else {
                            if (!value.equals("--DATOS_FAMILIARES--")) {
                                String[] s = value.split("=");
                                if (s.length == 2) {
                                    String attributeName = toCamelCase(s[0]);
                                    String attributeValue = s[1];
                                    load(datosFamiliares, attributeName, attributeValue);
                                }
                            }
                        }
                        if (i + 1 < values.length) {
                            value = values[++i];
                        } else {
                            if (datosPersona.getFamiliares() == null) {
                                datosPersona.setFamiliares(datosFamiliares);
                            }
                            break;
                        }
                    }
                    break;
                case "--POSIBLES_HERMANOS--":
                    List<PosiblesHermanos> hermanos = new ArrayList<PosiblesHermanos>();
                    PosiblesHermanos posiblesHermanos = null;
                    while (true) {
                        if (value.startsWith("--") && !value.equals("--POSIBLES_HERMANOS--")) {
                            datosPersona.setHermanos(hermanos);
                            i--;
                            break;
                        } else {
                            if (value.equals("--POSIBLES_HERMANOS--")) {
                                posiblesHermanos = new PosiblesHermanos();
                                hermanos.add(posiblesHermanos);
                            } else {
                                String[] s = value.split("=");
                                if (s.length == 2) {
                                    String attributeName = toCamelCase(s[0]);
                                    String attributeValue = s[1];
                                    load(posiblesHermanos, attributeName, attributeValue);
                                }
                            }
                        }
                        if (i + 1 < values.length) {
                            value = values[++i];
                        } else {
                            if (datosPersona.getHermanos() == null) {
                                datosPersona.setHermanos(hermanos);
                            }
                            break;
                        }
                    }
                    break;
                case "--PADRON_TSJE--":
                    List<PadronTsje> listPadron = new ArrayList<PadronTsje>();
                    PadronTsje elementPadron = null;
                    while (true) {
                        if (value.startsWith("--") && !value.equals("--PADRON_TSJE--")) {
                            datosPersona.setPadron(listPadron);
                            i--;
                            break;
                        } else {
                            if (value.equals("--PADRON_TSJE--")) {
                                elementPadron = new PadronTsje();
                                listPadron.add(elementPadron);
                            } else {
                                String[] s = value.split("=");
                                if (s.length == 2) {
                                    String attributeName = toCamelCase(s[0]);
                                    String attributeValue = s[1];
                                    load(elementPadron, attributeName, attributeValue);
                                }
                            }
                        }
                        if (i + 1 < values.length) {
                            value = values[++i];
                        } else {
                            if (datosPersona.getPadron() == null) {
                                datosPersona.setPadron(listPadron);
                            }
                            break;
                        }
                    }
                    break;
                case "--TELEFONOS_DE_CONTACTO--":
                    List<TelefonosDeContacto> list = new ArrayList<TelefonosDeContacto>();
                    TelefonosDeContacto element = null;
                    while (true) {
                        if (value.startsWith("--") && !value.equals("--TELEFONOS_DE_CONTACTO--")) {
                            datosPersona.setTelefonos(list);
                            i--;
                            break;
                        } else {
                            if (value.equals("--TELEFONOS_DE_CONTACTO--")) {
                                element = new TelefonosDeContacto();
                                list.add(element);
                            } else {
                                String[] s = value.split("=");
                                if (s.length == 2) {
                                    String attributeName = toCamelCase(s[0]);
                                    String attributeValue = s[1];
                                    load(element, attributeName, attributeValue);
                                }
                            }
                        }
                        if (i + 1 < values.length) {
                            value = values[++i];
                        } else {
                            if (datosPersona.getTelefonos() == null) {
                                datosPersona.setTelefonos(list);
                            }
                            break;
                        }
                    }
                    break;
                case "--SET--":
                    List<Set> listSet = new ArrayList<Set>();
                    Set elementSet = null;
                    while (true) {
                        if (value.startsWith("--") && !value.equals("--SET--")) {
                            datosPersona.setSet(listSet);
                            i--;
                            break;
                        } else {
                            if (value.equals("--SET--")) {
                                elementSet = new Set();
                                listSet.add(elementSet);
                            } else {
                                String[] s = value.split("=");
                                if (s.length == 2) {
                                    String attributeName = toCamelCase(s[0]);
                                    String attributeValue = s[1];
                                    load(elementSet, attributeName, attributeValue);
                                }
                            }
                        }
                        if (i + 1 < values.length) {
                            value = values[++i];
                        } else {
                            if (datosPersona.getSet() == null) {
                                datosPersona.setSet(listSet);
                            }
                            break;
                        }
                    }
                    break;
                case "--IPS--":
                    List<Ips> listIps = new ArrayList<Ips>();
                    Ips elementIps = null;
                    while (true) {
                        if (value.startsWith("--") && !value.equals("--IPS--")) {
                            datosPersona.setIps(listIps);
                            i--;
                            break;
                        } else {
                            if (value.equals("--IPS--")) {
                                elementIps = new Ips();
                                listIps.add(elementIps);
                            } else {
                                String[] s = value.split("=");
                                if (s.length == 2) {
                                    String attributeName = toCamelCase(s[0]);
                                    String attributeValue = s[1];
                                    load(elementIps, attributeName, attributeValue);
                                }
                            }
                        }
                        if (i + 1 < values.length) {
                            value = values[++i];
                        } else {
                            if (datosPersona.getIps()== null) {
                                datosPersona.setIps(listIps);
                            }
                            break;
                        }
                    }
                    break;
                    
                   case "--LISTA_NEGRA--":
                    List<ListaNegra> listNegra = new ArrayList<ListaNegra>();
                    ListaNegra elementNegra = null;
                    while (true) {
                        if (value.startsWith("--") && !value.equals("--LISTA_NEGRA--")) {
                            datosPersona.setListaNegra(listNegra);
                            i--;
                            break;
                        } else {
                            if (value.equals("--LISTA_NEGRA--")) {
                                elementNegra = new ListaNegra();
                                listNegra.add(elementNegra);
                            } else {
                                String[] s = value.split("=");
                                if (s.length == 2) {
                                    String attributeName = toCamelCase(s[0]);
                                    String attributeValue = s[1];
                                    load(elementNegra, attributeName, attributeValue);
                                }
                            }
                        }
                        if (i + 1 < values.length) {
                            value = values[++i];
                        } else {
                            if (datosPersona.getListaNegra()== null) {
                                datosPersona.setListaNegra(listNegra);
                            }
                            break;
                        }
                    }
                    break; 
                     
                     case "--ANTECEDENTES_POLICIALES--":
                     List<AntecedentesPolicial> listApoli = new ArrayList<AntecedentesPolicial>();
                    AntecedentesPolicial elementApoli = null;
                    while (true) {
                        if (value.startsWith("--") && !value.equals("--ANTECEDENTES_POLICIALES--")) {
                            datosPersona.setAntecedentesPolicial(listApoli);
                            i--;
                            break;
                        } else {
                            if (value.equals("--ANTECEDENTES_POLICIALES--")) {
                                elementApoli = new AntecedentesPolicial();
                                listApoli.add(elementApoli);
                            } else {
                                String[] s = value.split("=");
                                if (s.length == 2) {
                                    String attributeName = toCamelCase(s[0]);
                                    String attributeValue = s[1];
                                    load(elementApoli, attributeName, attributeValue);
                                }
                            }
                        }
                        if (i + 1 < values.length) {
                            value = values[++i];
                        } else {
                            if (datosPersona.getAntecedentesPolicial()== null) {
                                datosPersona.setAntecedentesPolicial(listApoli);
                            }
                            break;
                        }
                    }
                    break; 
                   
                        
                    case "--BNF_FUNCION_PUBLICA_SALARIO--":
                    List<BNFSalario> bnfSalario = new ArrayList<BNFSalario>();
                    BNFSalario elementbnfs = null;
                    while (true) {
                        if (value.startsWith("--") && !value.equals("--BNF_FUNCION_PUBLICA_SALARIO--")) {
                            datosPersona.setBnfSalario(bnfSalario);
                            i--;
                            break;
                        } else {
                            if (value.equals("--BNF_FUNCION_PUBLICA_SALARIO--")) {
                                elementbnfs = new BNFSalario();
                                bnfSalario.add(elementbnfs);
                            } else {
                                String[] s = value.split("=");
                                if (s.length == 2) {
                                    String attributeName = toCamelCase(s[0]);
                                    String attributeValue = s[1];
                                    load(elementbnfs, attributeName, attributeValue);
                                }
                            }
                        }
                        if (i + 1 < values.length) {
                            value = values[++i];
                        } else {
                            if (datosPersona.getBnfSalario()== null) {
                                datosPersona.setBnfSalario(bnfSalario);
                            }
                            break;
                        }
                    }
                    break;
                    case "--REFERENCIAS_JUDICIALES--":
                    List<ReferenciasJudiciales> lrefjudicial = new ArrayList<ReferenciasJudiciales>();
                    ReferenciasJudiciales elementrefjudi = null;
                    while (true) {
                        if (value.startsWith("--") && !value.equals("--REFERENCIAS_JUDICIALES--")) {
                            datosPersona.setReferenciasJudiciales(lrefjudicial);
                            i--;
                            break;
                        } else {
                            if (value.equals("--REFERENCIAS_JUDICIALES--")) {
                                elementrefjudi = new ReferenciasJudiciales();
                                lrefjudicial.add(elementrefjudi);
                            } else {
                                String[] s = value.split("=");
                                if (s.length == 2) {
                                    String attributeName = toCamelCase(s[0]);
                                    String attributeValue = s[1];
                                    load(elementrefjudi, attributeName, attributeValue);
                                }
                            }
                        }
                        if (i + 1 < values.length) {
                            value = values[++i];
                        } else {
                            if (datosPersona.getReferenciasJudiciales()== null) {
                                datosPersona.setReferenciasJudiciales(lrefjudicial);
                            }
                            break;
                        }
                    }
                    break;
             case "--DISOLUCION_CONYUGAL--":
                     List<DisolucionConyugal> listdisc = new ArrayList<DisolucionConyugal>();
                    DisolucionConyugal elementDisc= null;
                    while (true) {
                        if (value.startsWith("--") && !value.equals("--DISOLUCION_CONYUGAL--")) {
                            datosPersona.setDisolucionConyugal(listdisc);
                            i--;
                            break;
                        } else {
                            if (value.equals("--DISOLUCION_CONYUGAL--")) {
                                elementDisc = new DisolucionConyugal();
                                listdisc.add(elementDisc);
                            } else {
                                String[] s = value.split("=");
                                if (s.length == 2) {
                                    String attributeName = toCamelCase(s[0]);
                                    String attributeValue = s[1];
                                    load(elementDisc, attributeName, attributeValue);
                                }
                            }
                        }
                        if (i + 1 < values.length) {
                            value = values[++i];
                        } else {
                            if (datosPersona.getDisolucionConyugal()== null) {
                                datosPersona.setDisolucionConyugal(listdisc);
                            }
                            break;
                        }
                    }
                    break; 
                   
                        
             case "--OPERACIONES_BANCARIAS--":
                    List<OperacionesBancarias> lopebanc = new ArrayList<OperacionesBancarias>();
                    OperacionesBancarias elementreopebanc = null;
                    while (true) {
                        if (value.startsWith("--") && !value.equals("--OPERACIONES_BANCARIAS--")) {
                            datosPersona.setOperacionesBancarias(lopebanc);
                            i--;
                            break;
                        } else {
                            if (value.equals("--OPERACIONES_BANCARIAS--")) {
                                elementreopebanc = new OperacionesBancarias();
                                lopebanc.add(elementreopebanc);
                            } else {
                                String[] s = value.split("=");
                                if (s.length == 2) {
                                    String attributeName = toCamelCase(s[0]);
                                    String attributeValue = s[1];
                                    load(elementreopebanc, attributeName, attributeValue);
                                }
                            }
                        }
                        if (i + 1 < values.length) {
                            value = values[++i];
                        } else {
                            if (datosPersona.getOperacionesBancarias()== null) {
                                datosPersona.setOperacionesBancarias(lopebanc);
                            }
                            break;
                        }
                    }
                    break;  
            case "--REGISTRO_ABOGADOS--":
                     List<RegistrosAbogados> listsboga = new ArrayList<RegistrosAbogados>();
                    RegistrosAbogados elementAboga= null;
                    while (true) {
                        if (value.startsWith("--") && !value.equals("--REGISTRO_ABOGADOS--")) {
                            datosPersona.setRegistroAbogados(listsboga);
                            i--;
                            break;
                        } else {
                            if (value.equals("--REGISTRO_ABOGADOS--")) {
                                elementDisc = new DisolucionConyugal();
                                listsboga.add(elementAboga);
                            } else {
                                String[] s = value.split("=");
                                if (s.length == 2) {
                                    String attributeName = toCamelCase(s[0]);
                                    String attributeValue = s[1];
                                    load(elementAboga, attributeName, attributeValue);
                                }
                            }
                        }
                        if (i + 1 < values.length) {
                            value = values[++i];
                        } else {
                            if (datosPersona.getRegistroAbogados()== null) {
                                datosPersona.setRegistroAbogados(listsboga);
                            }
                            break;
                        }
                    }
                    break; 
             case "--PPR_HERMANOS--":
                    List<PPRHermano> lpprHermano = new ArrayList<PPRHermano>();
                    PPRHermano pprhermano = null;
                    while (true) {
                        this.logger.debug(value);
                        if (value.startsWith("--") && !value.equals("--PPR_HERMANOS--")) {
                            datosPersona.setPprHermano(lpprHermano);
                            i--;
                            break;
                        } else {
                            if (value.equals("--PPR_HERMANOS--")) {
                                pprhermano = new PPRHermano();
                                lpprHermano.add(pprhermano);
                            } else {
                                String[] s = value.split("=");
                                if (s.length == 2) {
                                    String attributeName = toCamelCase(s[0]);
                                    String attributeValue = s[1];
                                    load(pprhermano, attributeName, attributeValue);
                                }
                            }
                        }
                        if (i + 1 < values.length) {
                            value = values[++i];
                        } else {
                            if (datosPersona.getPprHermano()== null) {
                                datosPersona.setPprHermano(lpprHermano);
                            }
                            break;
                        }
                    }
                    break;  
                    
             case "--FALLECIDO--":
                    List<Fallecido> lfallecido = new ArrayList<Fallecido>();
                    Fallecido falle = null;
                    while (true) {
                        this.logger.debug(value);
                        if (value.startsWith("--") && !value.equals("--FALLECIDO--")) {
                            datosPersona.setFallecido(lfallecido);
                            i--;
                            break;
                        } else {
                            if (value.equals("--FALLECIDO--")) {
                                falle = new Fallecido();
                                lfallecido.add(falle);
                            } else {
                                String[] s = value.split("=");
                                if (s.length == 2) {
                                    String attributeName = toCamelCase(s[0]);
                                    String attributeValue = s[1];
                                    load(falle, attributeName, attributeValue);
                                }
                            }
                        }
                        if (i + 1 < values.length) {
                            value = values[++i];
                        } else {
                            if (datosPersona.getFallecido()== null) {
                                datosPersona.setFallecido(lfallecido);
                            }
                            break;
                        }
                    }
                    break;    
            case "--HISTORIAL_CONSULTAS--":
                    List<HistorialConsulta> lhistcon = new ArrayList<HistorialConsulta>();
                    HistorialConsulta histcon = null;
                    while (true) {
                        this.logger.debug(value);
                        if (value.startsWith("--") && !value.equals("--HISTORIAL_CONSULTAS--")) {
                            datosPersona.setHistorialConsulta(lhistcon);
                            i--;
                            break;
                        } else {
                            if (value.equals("--HISTORIAL_CONSULTAS--")) {
                                histcon = new HistorialConsulta();
                                lhistcon.add(histcon);
                            } else {
                                String[] s = value.split("=");
                                if (s.length == 2) {
                                    String attributeName = toCamelCase(s[0]);
                                    String attributeValue = s[1];
                                    load(histcon, attributeName, attributeValue);
                                }
                            }
                        }
                        if (i + 1 < values.length) {
                            value = values[++i];
                        } else {
                            if (datosPersona.getHistorialConsulta()== null) {
                                datosPersona.setHistorialConsulta(lhistcon);
                            }
                            break;
                        }
                    }
                    break;
            case "--PPR_HIJOS--":
                    List<PPRHijos> lhijos = new ArrayList<PPRHijos>();
                    PPRHijos hijos = null;
                    while (true) {
                        this.logger.debug(value);
                        if (value.startsWith("--") && !value.equals("--PPR_HIJOS--")) {
                            datosPersona.setPprHijos(lhijos);
                            i--;
                            break;
                        } else {
                            if (value.equals("--PPR_HIJOS--")) {
                                hijos = new PPRHijos();
                                lhijos.add(hijos);
                            } else {
                                String[] s = value.split("=");
                                if (s.length == 2) {
                                    String attributeName = toCamelCase(s[0]);
                                    String attributeValue = s[1];
                                    load(hijos, attributeName, attributeValue);
                                }
                            }
                        }
                        if (i + 1 < values.length) {
                            value = values[++i];
                        } else {
                            if (datosPersona.getPprHijos()== null) {
                                datosPersona.setPprHijos(lhijos);
                            }
                            break;
                        }
                    }
                    break; 
           case "--INFORME_PDF--":
                   // InformePdf pdf =  new InformePdf();
                    while (true) {
                        if (value.startsWith("--") && !value.equals("--INFORME_PDF--")) {
                            i--;
                            break;
                        } else {
                            
                            if (!value.equals("--INFORME_PDF--")) {
                                String[] s = value.split(" = ");
                                if (s.length == 2) {
                                    String attributeName = toCamelCase(s[0]);
                                    String attributeValue =  s[1].trim();
                                    logger.info(attributeName+">>"+attributeValue);
                                    //load(datosPersona, attributeName, attributeValue);
                                    
                                    datosPersona.setPdf_url(attributeValue);
                                }
                            }
                        }
                        if (i + 1 < values.length) {
                            value = values[++i];
                        } else {
                            break;
                        }
                    }
                    break;      
            }
        }
        logger.info("OUT: [" + datosPersona + "]");
        return datosPersona;
    }
    public DatosPersona parsear(String str) {
        logger.info("IN: [" + str + "]");
        String[] values = str.split(";;");
        DatosPersona datosPersona = new DatosPersona();

        for (int i = 0; i < values.length; i++) {
            String value = values[i];
            if (value.contains("--")) {
                value = value.trim();
            }
            switch (value) {
                  case "--RESULTADO--":
                    while (true) {
                        if (value.startsWith("--") && !value.equals("--RESULTADO--")) {
                            i--;
                            break;
                        } else {
                            if (!value.equals("--RESULTADO--")) {
                                String[] s = value.split("=");
                                if (s.length == 2) {
                                    String attributeName = toCamelCase(s[0]);
                                    String attributeValue =  s[1].trim();
                                    load(datosPersona, attributeName, attributeValue);
                                }
                            }
                        }
                        if (i + 1 < values.length) {
                            value = values[++i];
                        } else {
                            break;
                        }
                    }
                    break;
                case "--DATOS_PERSONA--":
                    while (true) {
                        if (value.startsWith("--") && !value.equals("--DATOS_PERSONA--")) {
                            i--;
                            break;
                        } else {
                            if (!value.equals("--DATOS_PERSONA--")) {
                                String[] s = value.split("=");
                                if (s.length == 2 ) {
                                    String attributeName = toCamelCase(s[0]);
                                    String attributeValue =  s[1].trim();
                                   if(!attributeName.equalsIgnoreCase("fotoPerfil")) {
                                       load(datosPersona, attributeName, attributeValue);
                                   }else{
                                     ImagenRostro igr =   new ImagenRostro(); 
                                     igr.setPng_url(attributeValue);
                                     datosPersona.setImagenRostro(igr);
                                     }
                                }
                            }
                        }
                        if (i + 1 < values.length) {
                            value = values[++i];
                        } else {
                            break;
                        }
                    }
                    break;
                case "--DATOS_FAMILIARES--":
                    DatosFamiliares datosFamiliares = new DatosFamiliares();
                    while (true) {
                        if (value.startsWith("--") && !value.equals("--DATOS_FAMILIARES--")) {
                            datosPersona.setFamiliares(datosFamiliares);
                            i--;
                            break;
                        } else {
                            if (!value.equals("--DATOS_FAMILIARES--")) {
                                String[] s = value.split("=");
                                if (s.length == 2) {
                                    String attributeName = toCamelCase(s[0]);
                                    String attributeValue = s[1];
                                    load(datosFamiliares, attributeName, attributeValue);
                                }
                            }
                        }
                        if (i + 1 < values.length) {
                            value = values[++i];
                        } else {
                            if (datosPersona.getFamiliares() == null) {
                                datosPersona.setFamiliares(datosFamiliares);
                            }
                            break;
                        }
                    }
                    break;
                case "--POSIBLES_HERMANOS--":
                    List<PosiblesHermanos> hermanos = new ArrayList<PosiblesHermanos>();
                    PosiblesHermanos posiblesHermanos = null;
                    while (true) {
                        if (value.startsWith("--") && !value.equals("--POSIBLES_HERMANOS--")) {
                            datosPersona.setHermanos(hermanos);
                            i--;
                            break;
                        } else {
                            if (value.equals("--POSIBLES_HERMANOS--")) {
                                posiblesHermanos = new PosiblesHermanos();
                                hermanos.add(posiblesHermanos);
                            } else {
                                String[] s = value.split("=");
                                if (s.length == 2) {
                                    String attributeName = toCamelCase(s[0]);
                                    String attributeValue = s[1];
                                    load(posiblesHermanos, attributeName, attributeValue);
                                }
                            }
                        }
                        if (i + 1 < values.length) {
                            value = values[++i];
                        } else {
                            if (datosPersona.getHermanos() == null) {
                                datosPersona.setHermanos(hermanos);
                            }
                            break;
                        }
                    }
                    break;
                case "--PADRON_TSJE--":
                    List<PadronTsje> listPadron = new ArrayList<PadronTsje>();
                    PadronTsje elementPadron = null;
                    while (true) {
                        if (value.startsWith("--") && !value.equals("--PADRON_TSJE--")) {
                            datosPersona.setPadron(listPadron);
                            i--;
                            break;
                        } else {
                            if (value.equals("--PADRON_TSJE--")) {
                                elementPadron = new PadronTsje();
                                listPadron.add(elementPadron);
                            } else {
                                String[] s = value.split("=");
                                if (s.length == 2) {
                                    String attributeName = toCamelCase(s[0]);
                                    String attributeValue = s[1];
                                    load(elementPadron, attributeName, attributeValue);
                                }
                            }
                        }
                        if (i + 1 < values.length) {
                            value = values[++i];
                        } else {
                            if (datosPersona.getPadron() == null) {
                                datosPersona.setPadron(listPadron);
                            }
                            break;
                        }
                    }
                    break;
                case "--TELEFONOS_DE_CONTACTO--":
                    List<TelefonosDeContacto> list = new ArrayList<TelefonosDeContacto>();
                    TelefonosDeContacto element = null;
                    while (true) {
                        if (value.startsWith("--") && !value.equals("--TELEFONOS_DE_CONTACTO--")) {
                            datosPersona.setTelefonos(list);
                            i--;
                            break;
                        } else {
                            if (value.equals("--TELEFONOS_DE_CONTACTO--")) {
                                element = new TelefonosDeContacto();
                                list.add(element);
                            } else {
                                String[] s = value.split("=");
                                if (s.length == 2) {
                                    String attributeName = toCamelCase(s[0]);
                                    String attributeValue = s[1];
                                    load(element, attributeName, attributeValue);
                                }
                            }
                        }
                        if (i + 1 < values.length) {
                            value = values[++i];
                        } else {
                            if (datosPersona.getTelefonos() == null) {
                                datosPersona.setTelefonos(list);
                            }
                            break;
                        }
                    }
                    break;
                case "--SET--":
                    List<Set> listSet = new ArrayList<Set>();
                    Set elementSet = null;
                    while (true) {
                        if (value.startsWith("--") && !value.equals("--SET--")) {
                            datosPersona.setSet(listSet);
                            i--;
                            break;
                        } else {
                            if (value.equals("--SET--")) {
                                elementSet = new Set();
                                listSet.add(elementSet);
                            } else {
                                String[] s = value.split("=");
                                if (s.length == 2) {
                                    String attributeName = toCamelCase(s[0]);
                                    String attributeValue = s[1];
                                    load(elementSet, attributeName, attributeValue);
                                }
                            }
                        }
                        if (i + 1 < values.length) {
                            value = values[++i];
                        } else {
                            if (datosPersona.getSet() == null) {
                                datosPersona.setSet(listSet);
                            }
                            break;
                        }
                    }
                    break;
                case "--IPS--":
                    List<Ips> listIps = new ArrayList<Ips>();
                    Ips elementIps = null;
                    while (true) {
                        if (value.startsWith("--") && !value.equals("--IPS--")) {
                            datosPersona.setIps(listIps);
                            i--;
                            break;
                        } else {
                            if (value.equals("--IPS--")) {
                                elementIps = new Ips();
                                listIps.add(elementIps);
                            } else {
                                String[] s = value.split("=");
                                if (s.length == 2) {
                                    String attributeName = toCamelCase(s[0]);
                                    String attributeValue = s[1];
                                    load(elementIps, attributeName, attributeValue);
                                }
                            }
                        }
                        if (i + 1 < values.length) {
                            value = values[++i];
                        } else {
                            if (datosPersona.getIps()== null) {
                                datosPersona.setIps(listIps);
                            }
                            break;
                        }
                    }
                    break;
                    case "--IPS_V2_DETALLE_BASICO--":
                    List<IpsV2DetalleBasico> listIpsV2DetalleBasico = new ArrayList<IpsV2DetalleBasico>();
                    IpsV2DetalleBasico elementlistIpsV2DetalleBasico = null;
                    while (true) {
                        if (value.startsWith("--") && !value.equals("--IPS_V2_DETALLE_BASICO--")) {
                            datosPersona.setIpsV2DetalleBasico(listIpsV2DetalleBasico);
                            i--;
                            break;
                        } else {
                            if (value.equals("--IPS_V2_DETALLE_BASICO--")) {
                                elementlistIpsV2DetalleBasico = new IpsV2DetalleBasico();
                                listIpsV2DetalleBasico.add(elementlistIpsV2DetalleBasico);
                            } else {
                                String[] s = value.split("=");
                                if (s.length == 2) {
                                    String attributeName = toCamelCase(s[0]);
                                    String attributeValue = s[1];
                                    load(elementlistIpsV2DetalleBasico, attributeName, attributeValue);
                                }
                            }
                        }
                        if (i + 1 < values.length) {
                            value = values[++i];
                        } else {
                            if (datosPersona.getIpsV2DetalleBasico()== null) {
                                datosPersona.setIpsV2DetalleBasico(listIpsV2DetalleBasico);
                            }
                            break;
                        }
                    }
                    break; 
                    
                    case "--IPS_V2_DETALLE_PATRONAL--":
                    List<IpsV2DetallePatronal> listIpsV2DetallePatronal = new ArrayList<IpsV2DetallePatronal>();
                    IpsV2DetallePatronal elementlistIpsV2DetallePatronal = null;
                    while (true) {
                        if (value.startsWith("--") && !value.equals("--IPS_V2_DETALLE_PATRONAL--")) {
                            datosPersona.setIpsV2DetallePatronal(listIpsV2DetallePatronal);
                            i--;
                            break;
                        } else {
                            if (value.equals("--IPS_V2_DETALLE_PATRONAL--")) {
                                elementlistIpsV2DetallePatronal = new IpsV2DetallePatronal();
                                listIpsV2DetallePatronal.add(elementlistIpsV2DetallePatronal);
                            } else {
                                String[] s = value.split("=");
                                if (s.length == 2) {
                                    String attributeName = toCamelCase(s[0]);
                                    String attributeValue = s[1];
                                    load(elementlistIpsV2DetallePatronal, attributeName, attributeValue);
                                }
                            }
                        }
                        if (i + 1 < values.length) {
                            value = values[++i];
                        } else {
                            if (datosPersona.getIpsV2DetallePatronal()== null) {
                                datosPersona.setIpsV2DetallePatronal(listIpsV2DetallePatronal);
                            }
                            break;
                        }
                    }
                    break; 
                    
                     case "--IPS_V2_DETALLE_BENEFICIARIO--":
                    List<IpsV2DetalleBeneficiario> listIpsV2DetalleBeneficiario = new ArrayList<IpsV2DetalleBeneficiario>();
                    IpsV2DetalleBeneficiario elementlistIpsV2DetalleBeneficiario = null;
                    while (true) {
                        if (value.startsWith("--") && !value.equals("--IPS_V2_DETALLE_BENEFICIARIO--")) {
                            datosPersona.setIpsV2DetalleBeneficiario(listIpsV2DetalleBeneficiario);
                            i--;
                            break;
                        } else {
                            if (value.equals("--IPS_V2_DETALLE_BENEFICIARIO--")) {
                                elementlistIpsV2DetalleBeneficiario = new IpsV2DetalleBeneficiario();
                                listIpsV2DetalleBeneficiario.add(elementlistIpsV2DetalleBeneficiario);
                            } else {
                                String[] s = value.split("=");
                                if (s.length == 2) {
                                    String attributeName = toCamelCase(s[0]);
                                    String attributeValue = s[1];
                                    load(elementlistIpsV2DetalleBeneficiario, attributeName, attributeValue);
                                }
                            }
                        }
                        if (i + 1 < values.length) {
                            value = values[++i];
                        } else {
                            if (datosPersona.getIpsV2DetalleBeneficiario()== null) {
                                datosPersona.setIpsV2DetalleBeneficiario(listIpsV2DetalleBeneficiario);
                            }
                            break;
                        }
                    }
                    break; 
                    
                   case "--LISTA_NEGRA--":
                    List<ListaNegra> listNegra = new ArrayList<ListaNegra>();
                    ListaNegra elementNegra = null;
                    while (true) {
                        if (value.startsWith("--") && !value.equals("--LISTA_NEGRA--")) {
                            datosPersona.setListaNegra(listNegra);
                            i--;
                            break;
                        } else {
                            if (value.equals("--LISTA_NEGRA--")) {
                                elementNegra = new ListaNegra();
                                listNegra.add(elementNegra);
                            } else {
                                String[] s = value.split("=");
                                if (s.length == 2) {
                                    String attributeName = toCamelCase(s[0]);
                                    String attributeValue = s[1];
                                    load(elementNegra, attributeName, attributeValue);
                                }
                            }
                        }
                        if (i + 1 < values.length) {
                            value = values[++i];
                        } else {
                            if (datosPersona.getListaNegra()== null) {
                                datosPersona.setListaNegra(listNegra);
                            }
                            break;
                        }
                    }
                    break; 
                     
                     case "--ANTECEDENTES_POLICIALES--":
                     List<AntecedentesPolicial> listApoli = new ArrayList<AntecedentesPolicial>();
                    AntecedentesPolicial elementApoli = null;
                    while (true) {
                        if (value.startsWith("--") && !value.equals("--ANTECEDENTES_POLICIALES--")) {
                            datosPersona.setAntecedentesPolicial(listApoli);
                            i--;
                            break;
                        } else {
                            if (value.equals("--ANTECEDENTES_POLICIALES--")) {
                                elementApoli = new AntecedentesPolicial();
                                listApoli.add(elementApoli);
                            } else {
                                String[] s = value.split("=");
                                if (s.length == 2) {
                                    String attributeName = toCamelCase(s[0]);
                                    String attributeValue = s[1];
                                    load(elementApoli, attributeName, attributeValue);
                                }
                            }
                        }
                        if (i + 1 < values.length) {
                            value = values[++i];
                        } else {
                            if (datosPersona.getAntecedentesPolicial()== null) {
                                datosPersona.setAntecedentesPolicial(listApoli);
                            }
                            break;
                        }
                    }
                    break; 
                   
                        
                    case "--BNF_FUNCION_PUBLICA_SALARIO--":
                    List<BNFSalario> bnfSalario = new ArrayList<BNFSalario>();
                    BNFSalario elementbnfs = null;
                    while (true) {
                        if (value.startsWith("--") && !value.equals("--BNF_FUNCION_PUBLICA_SALARIO--")) {
                            datosPersona.setBnfSalario(bnfSalario);
                            i--;
                            break;
                        } else {
                            if (value.equals("--BNF_FUNCION_PUBLICA_SALARIO--")) {
                                elementbnfs = new BNFSalario();
                                bnfSalario.add(elementbnfs);
                            } else {
                                String[] s = value.split("=");
                                if (s.length == 2) {
                                    String attributeName = toCamelCase(s[0]);
                                    String attributeValue = s[1];
                                    load(elementbnfs, attributeName, attributeValue);
                                }
                            }
                        }
                        if (i + 1 < values.length) {
                            value = values[++i];
                        } else {
                            if (datosPersona.getBnfSalario()== null) {
                                datosPersona.setBnfSalario(bnfSalario);
                            }
                            break;
                        }
                    }
                    break;
                    case "--REFERENCIAS_JUDICIALES--":
                    List<ReferenciasJudiciales> lrefjudicial = new ArrayList<ReferenciasJudiciales>();
                    ReferenciasJudiciales elementrefjudi = null;
                    while (true) {
                        if (value.startsWith("--") && !value.equals("--REFERENCIAS_JUDICIALES--")) {
                            datosPersona.setReferenciasJudiciales(lrefjudicial);
                            i--;
                            break;
                        } else {
                            if (value.equals("--REFERENCIAS_JUDICIALES--")) {
                                elementrefjudi = new ReferenciasJudiciales();
                                lrefjudicial.add(elementrefjudi);
                            } else {
                                String[] s = value.split("=");
                                if (s.length == 2) {
                                    String attributeName = toCamelCase(s[0]);
                                    String attributeValue = s[1];
                                    load(elementrefjudi, attributeName, attributeValue);
                                }
                            }
                        }
                        if (i + 1 < values.length) {
                            value = values[++i];
                        } else {
                            if (datosPersona.getReferenciasJudiciales()== null) {
                                datosPersona.setReferenciasJudiciales(lrefjudicial);
                            }
                            break;
                        }
                    }
                    break;
             case "--DISOLUCION_CONYUGAL--":
                     List<DisolucionConyugal> listdisc = new ArrayList<DisolucionConyugal>();
                    DisolucionConyugal elementDisc= null;
                    while (true) {
                        if (value.startsWith("--") && !value.equals("--DISOLUCION_CONYUGAL--")) {
                            datosPersona.setDisolucionConyugal(listdisc);
                            i--;
                            break;
                        } else {
                            if (value.equals("--DISOLUCION_CONYUGAL--")) {
                                elementDisc = new DisolucionConyugal();
                                listdisc.add(elementDisc);
                            } else {
                                String[] s = value.split("=");
                                if (s.length == 2) {
                                    String attributeName = toCamelCase(s[0]);
                                    String attributeValue = s[1];
                                    load(elementDisc, attributeName, attributeValue);
                                }
                            }
                        }
                        if (i + 1 < values.length) {
                            value = values[++i];
                        } else {
                            if (datosPersona.getDisolucionConyugal()== null) {
                                datosPersona.setDisolucionConyugal(listdisc);
                            }
                            break;
                        }
                    }
                    break; 
                   
                        
             case "--OPERACIONES_BANCARIAS--":
                    List<OperacionesBancarias> lopebanc = new ArrayList<OperacionesBancarias>();
                    OperacionesBancarias elementreopebanc = null;
                    while (true) {
                        if (value.startsWith("--") && !value.equals("--OPERACIONES_BANCARIAS--")) {
                            datosPersona.setOperacionesBancarias(lopebanc);
                            i--;
                            break;
                        } else {
                            if (value.equals("--OPERACIONES_BANCARIAS--")) {
                                elementreopebanc = new OperacionesBancarias();
                                lopebanc.add(elementreopebanc);
                            } else {
                                String[] s = value.split("=");
                                if (s.length == 2) {
                                    String attributeName = toCamelCase(s[0]);
                                    String attributeValue = s[1];
                                    load(elementreopebanc, attributeName, attributeValue);
                                }
                            }
                        }
                        if (i + 1 < values.length) {
                            value = values[++i];
                        } else {
                            if (datosPersona.getOperacionesBancarias()== null) {
                                datosPersona.setOperacionesBancarias(lopebanc);
                            }
                            break;
                        }
                    }
                    break;  
            case "--REGISTRO_ABOGADOS--":
                     List<RegistrosAbogados> listsboga = new ArrayList<RegistrosAbogados>();
                    RegistrosAbogados elementAboga= null;
                    while (true) {
                        if (value.startsWith("--") && !value.equals("--REGISTRO_ABOGADOS--")) {
                            datosPersona.setRegistroAbogados(listsboga);
                            i--;
                            break;
                        } else {
                            if (value.equals("--REGISTRO_ABOGADOS--")) {
                                elementDisc = new DisolucionConyugal();
                                listsboga.add(elementAboga);
                            } else {
                                String[] s = value.split("=");
                                if (s.length == 2) {
                                    String attributeName = toCamelCase(s[0]);
                                    String attributeValue = s[1];
                                    load(elementAboga, attributeName, attributeValue);
                                }
                            }
                        }
                        if (i + 1 < values.length) {
                            value = values[++i];
                        } else {
                            if (datosPersona.getRegistroAbogados()== null) {
                                datosPersona.setRegistroAbogados(listsboga);
                            }
                            break;
                        }
                    }
                    break; 
             case "--PPR_HERMANOS--":
                    List<PPRHermano> lpprHermano = new ArrayList<PPRHermano>();
                    PPRHermano pprhermano = null;
                    while (true) {
                        this.logger.debug(value);
                        if (value.startsWith("--") && !value.equals("--PPR_HERMANOS--")) {
                            datosPersona.setPprHermano(lpprHermano);
                            i--;
                            break;
                        } else {
                            if (value.equals("--PPR_HERMANOS--")) {
                                pprhermano = new PPRHermano();
                                lpprHermano.add(pprhermano);
                            } else {
                                String[] s = value.split("=");
                                if (s.length == 2) {
                                    String attributeName = toCamelCase(s[0]);
                                    String attributeValue = s[1];
                                    load(pprhermano, attributeName, attributeValue);
                                }
                            }
                        }
                        if (i + 1 < values.length) {
                            value = values[++i];
                        } else {
                            if (datosPersona.getPprHermano()== null) {
                                datosPersona.setPprHermano(lpprHermano);
                            }
                            break;
                        }
                    }
                    break;  
                    
             case "--FALLECIDO--":
                    List<Fallecido> lfallecido = new ArrayList<Fallecido>();
                    Fallecido falle = null;
                    while (true) {
                        this.logger.debug(value);
                        if (value.startsWith("--") && !value.equals("--FALLECIDO--")) {
                            datosPersona.setFallecido(lfallecido);
                            i--;
                            break;
                        } else {
                            if (value.equals("--FALLECIDO--")) {
                                falle = new Fallecido();
                                lfallecido.add(falle);
                            } else {
                                String[] s = value.split("=");
                                if (s.length == 2) {
                                    String attributeName = toCamelCase(s[0]);
                                    String attributeValue = s[1];
                                    load(falle, attributeName, attributeValue);
                                }
                            }
                        }
                        if (i + 1 < values.length) {
                            value = values[++i];
                        } else {
                            if (datosPersona.getFallecido()== null) {
                                datosPersona.setFallecido(lfallecido);
                            }
                            break;
                        }
                    }
                    break;    
            case "--HISTORIAL_CONSULTAS--":
                    List<HistorialConsulta> lhistcon = new ArrayList<HistorialConsulta>();
                    HistorialConsulta histcon = null;
                    while (true) {
                        this.logger.debug(value);
                        if (value.startsWith("--") && !value.equals("--HISTORIAL_CONSULTAS--")) {
                            datosPersona.setHistorialConsulta(lhistcon);
                            i--;
                            break;
                        } else {
                            if (value.equals("--HISTORIAL_CONSULTAS--")) {
                                histcon = new HistorialConsulta();
                                lhistcon.add(histcon);
                            } else {
                                String[] s = value.split("=");
                                if (s.length == 2) {
                                    String attributeName = toCamelCase(s[0]);
                                    String attributeValue = s[1];
                                    load(histcon, attributeName, attributeValue);
                                }
                            }
                        }
                        if (i + 1 < values.length) {
                            value = values[++i];
                        } else {
                            if (datosPersona.getHistorialConsulta()== null) {
                                datosPersona.setHistorialConsulta(lhistcon);
                            }
                            break;
                        }
                    }
                    break;
            case "--PPR_HIJOS--":
                    List<PPRHijos> lhijos = new ArrayList<PPRHijos>();
                    PPRHijos hijos = null;
                    while (true) {
                        this.logger.debug(value);
                        if (value.startsWith("--") && !value.equals("--PPR_HIJOS--")) {
                            datosPersona.setPprHijos(lhijos);
                            i--;
                            break;
                        } else {
                            if (value.equals("--PPR_HIJOS--")) {
                                hijos = new PPRHijos();
                                lhijos.add(hijos);
                            } else {
                                String[] s = value.split("=");
                                if (s.length == 2) {
                                    String attributeName = toCamelCase(s[0]);
                                    String attributeValue = s[1];
                                    load(hijos, attributeName, attributeValue);
                                }
                            }
                        }
                        if (i + 1 < values.length) {
                            value = values[++i];
                        } else {
                            if (datosPersona.getPprHijos()== null) {
                                datosPersona.setPprHijos(lhijos);
                            }
                            break;
                        }
                    }
                    break; 
                    
                  case "--REDAM_DETALLE--":
                    RedamDetalle redamDetalle = new RedamDetalle();
                    while (true) {
                        if (value.startsWith("--") && !value.equals("--REDAM_DETALLE--")) {
                            datosPersona.setRedamDetalle(redamDetalle);
                            i--;
                            break;
                        } else {
                            if (!value.equals("--REDAM_DETALLE--")) {
                                String[] s = value.split("=");
                                if (s.length == 2) {
                                    String attributeName = toCamelCase(s[0]);
                                    String attributeValue = s[1];
                                    load(redamDetalle, attributeName, attributeValue);
                                }
                            }
                        }
                        if (i + 1 < values.length) {
                            value = values[++i];
                        } else {
                            if (datosPersona.getRedamDetalle()== null) {
                                datosPersona.setRedamDetalle(redamDetalle);
                            }
                            break;
                        }
                    }
                    break;
            case "--CUSTOM_PJ--":
                    CustomPJ customPJ = new CustomPJ();
                    while (true) {
                        if (value.startsWith("--") && !value.equals("--CUSTOM_PJ--")) {
                            datosPersona.setCustomPJ(customPJ);
                            i--;
                            break;
                        } else {
                            if (!value.equals("--CUSTOM_PJ--")) {
                                String[] s = value.split("=");
                                if (s.length == 2) {
                                    String attributeName = toCamelCase(s[0]);
                                    String attributeValue = s[1];
                                    load(customPJ, attributeName, attributeValue);
                                }
                            }
                        }
                        if (i + 1 < values.length) {
                            value = values[++i];
                        } else {
                            if (datosPersona.getCustomPJ()== null) {
                                datosPersona.setCustomPJ(customPJ);
                            }
                            break;
                        }
                    }
                    break;        
                  
            case "--INFORME_PDF--":
                    InformePdf pdf =  new InformePdf();
                    while (true) {
                        if (value.startsWith("--") && !value.equals("--INFORME_PDF--")) {
                            i--;
                            break;
                        } else {
                            
                            if (!value.equals("--INFORME_PDF--")) {
                                String[] s = value.split(" = ");
                                if (s.length == 2) {
                                    String attributeName = toCamelCase(s[0]);
                                    String attributeValue =  s[1].trim();
                                    logger.info(attributeName+">>"+attributeValue);
                                    //load(datosPersona, attributeName, attributeValue);
                                    pdf.setPdf_url(attributeValue);
                                    
                                    datosPersona.setInformePdf(pdf);
                                }
                            }
                        }
                        if (i + 1 < values.length) {
                            value = values[++i];
                        } else {
                            break;
                        }
                    }
                    break;    
            }
        }
        logger.info("OUT: [" + datosPersona + "]");
        return datosPersona;
    }

    public String toCamelCaseClass(String s) {
        if (s.isEmpty()) {
            logger.error("Cadena Vacia");
            return "N/A";
        }
        try {
            String[] parts = s.split("_");
            String camelCaseString = "";
            for (String part : parts) {
                camelCaseString = camelCaseString + toProperCaseClass(part);
            }
            return camelCaseString;
        } catch (Exception e) {
            logger.error("Exception con [" + s + "]", e);
        }
        return "N/A";
    }

    public String toProperCaseClass(String s) {
        if (s.isEmpty()) {
            logger.error("Cadena Vacia");
            return "N/A";
        }
        try {
            return s.substring(0, 1).toUpperCase() + s.substring(1).toLowerCase();
        } catch (Exception e) {
            logger.error("Exception con [" + s + "]", e);
        }
        return "N/A";
    }

    public String toCamelCase(String s) {
        if (s.isEmpty()) {
            logger.error("Cadena Vacia");
            return "N/A";
        }
        try {
            String[] parts = s.split("_");
            String camelCaseString = "";
            for (String part : parts) {
                camelCaseString = camelCaseString + toProperCase(part);
            }
            camelCaseString = camelCaseString.substring(0, 1).toLowerCase() + camelCaseString.substring(1);
            return camelCaseString;
        } catch (Exception e) {
            logger.error("Exception con [" + s + "]", e);
        }
        return "N/A";
    }

    public String toProperCase(String s) {
        if (s.isEmpty()) {
            logger.error("Cadena Vacia");
            return "N/A";
        }
        try {
            return s.substring(0, 1).toUpperCase() + s.substring(1).toLowerCase();
        } catch (Exception e) {
            logger.error("Exception con [" + s + "]", e);
        }
        return "N/A";
    }

    private void load(Object objActual, String attributeName, String attributeValue) {
        this.logger.debug(attributeName+"::"+attributeValue);
        try {
            Class clazz = objActual.getClass();
            Field field = clazz.getDeclaredField(attributeName);
            field.setAccessible(true);
            field.set(objActual, attributeValue);
            field.setAccessible(false);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }
    
    
    
  
}
