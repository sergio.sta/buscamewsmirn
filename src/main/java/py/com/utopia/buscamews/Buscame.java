/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.utopia.buscamews;

import com.itextpdf.xmp.impl.Base64;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.imageio.ImageIO;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;

import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import static py.com.utopia.buscamews.Constantes.*;
import static py.com.utopia.buscamews.Constantes.URL_LOGIN;
import py.com.utopia.buscamews.dto.BuscameDTO;
import py.com.utopia.buscamews.dto.DatosPersona;
import py.com.utopia.buscamews.dto.DatosPersona2;
import py.com.utopia.buscamews.dto.ImagenRostro;
import py.com.utopia.buscamews.dto.InformePdf;


/**
 *
 * @author sergio
 */
@WebService(serviceName = "Buscame")
public class Buscame {

    private final Logger logger = LogManager.getLogger(this.getClass());

    /**
     *
     * @param usuario
     * @param password
     * @param cedula
     * @return
     */
    @WebMethod(operationName = "buscameDatos")
    public BuscameDTO buscameDatos(@WebParam(name = "usuario") String usuario, @WebParam(name = "password") String password, @WebParam(name = "cedula") String cedula, @WebParam(name = "tipo") String tipo) {
        logger.info("IN: [usuario={}; password={}; cedula={}; tipo={}]", usuario, password, cedula, tipo);
        BuscameDTO responseWS;
        if(tipo != null && !tipo.equals("basic") && !tipo.equals("full")){
            BuscameDTO b = new BuscameDTO();
            b.setEstado(-1);
            b.setMensaje("error de parametro tipo");
        }
        
        if(tipo==null) tipo = "full";
        
        
        try {
            HttpClient client = HttpClientBuilder.create().build();
            HttpPost post = new HttpPost(URL_LOGIN);
            //post.setHeader("User-Agent", "Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.124 Safari/537.36");
            List<NameValuePair> urlParameters = new ArrayList<NameValuePair>();
            urlParameters.add(new BasicNameValuePair("user[email]", usuario));
            urlParameters.add(new BasicNameValuePair("user[pass]", password));
            post.setEntity(new UrlEncodedFormEntity(urlParameters));
            org.apache.http.HttpResponse response = client.execute(post);
            logger.info("Enviando 'POST' a URL : {}", URL_LOGIN);
            logger.info("Parametros POST : [{};{}]", usuario, password);
            logger.info("Response: [{}]", response.getStatusLine().getStatusCode());
            if(!(response.getStatusLine().getStatusCode()==200 || response.getStatusLine().getStatusCode()==302 )){
                              BuscameDTO b = new BuscameDTO();
                            b.setEstado(response.getStatusLine().getStatusCode());
                            b.setMensaje("ERROR HTTP");
                            return b;
                            
                            
            }
            String phpCookie = response.getFirstHeader("Set-Cookie").getValue();
            String url = URL_DATA2.replaceAll("<cedula>", cedula).replaceAll("<tipo>", tipo);
            HttpGet get = new HttpGet(url);
           //HttpGet get = new HttpGet(URL_DATA3);
           get.setHeader("Cookie", phpCookie);
           get.setHeader("charset","utf-8");
           get.setHeader(null);
            response = client.execute(get);
            logger.info("Enviando 'GET' a URL : {}{}", url, cedula);
            logger.info("Parametros GET : [{}]", phpCookie);
            logger.info("Response: [{}]", response.getStatusLine().getStatusCode());
            if(!(response.getStatusLine().getStatusCode()==200 || response.getStatusLine().getStatusCode()==302 ) ){
                              BuscameDTO b = new BuscameDTO();
                            b.setEstado(response.getStatusLine().getStatusCode());
                            b.setMensaje("ERROR HTTP");
                            return b;
                            
                            
            }
            BufferedReader br = new BufferedReader(new InputStreamReader(response.getEntity().getContent(), "UTF-8"));
            String output;
            String responseS = "";
            while ((output = br.readLine()) != null) {
                responseS += output;
            }
            
           // logger.info(responseS);
            
            Document doc = Jsoup.parseBodyFragment(responseS);
           
            logger.info(doc.outputSettings().charset());
            String str = doc.body().toString();
            // Alarc&oacute;n   á &Aacute;
           str = str.replaceAll("&oacute;", "ó").replaceAll("&aacute;", "á").replaceAll("&Aacute;", "Á").replaceAll("&Oacute;", "Ó")
                   .replaceAll("&Ntilde;", "Ñ").replaceAll("&eacute;", "é").replaceAll("&Eacute;", " É").replaceAll("&iacute;", "í").replaceAll("&Iacute;", "Í").replaceAll("&ordm;", "°").replaceAll("amp;", "")
                   .replace("&uacute;", "ú").replace("&uacute;", "ú");
            logger.info(str);
            /*String str = "--DATOS_PERSONA--;;"
             + "estado=0;;"
             + "mensaje=exito;;"
             + "tipo=DNI;;"
             + "ultimo_registro_ips=holaaa;;"
             + "nacionalidad_origen=PARAGUAY;;"
             + "fecha_aprox_vto=01/10/2015;;"
             + "nro_documento=1.723.754;;"
             + "fecha_nac=08/09/1984;;"
             + "sexo=Masculino;;"
             + "estado_civ=SOLTERO/A;;"
             + "fecha_emision_ci=12/10/2009;;"
             + "domicilio=B° SANTA ANA - SAN PEDRO;;"
             + "nombre=EDGAR GILBERTO;;"
             + "edad=32;;"
             + "profesion=ESTUDIANTE;;"
             + "fecha_venc_ci=12/10/2019;;"
             + "barrio=SAN VICENTE;;"
             + "ciudad=ASUNCION;;"
             + "apellidos=VILLALBA CACERES;;"
             + "lugar=SAN PEDRO;;"
             + "nacionalidad=PARAGUAYA;;"
             + "nro_doc_extrangero=VACIO;;"
             + "fecha_grab_ci=05/10/2009;;"
             + "telefono=VACIO;;"
             + "--DATOS_FAMILIARES--;;"
             + "nro_doc_padre=VACIO;;"
             + "nombre_padre=ELVIO GILBERTO;;"
             + "apellido_padre=VACIO;;"
             + "nro_doc_madre=VACIO;;"
             + "nombre_madre=MARIA CESARIA;;"
             + "apellido_madre=VACIO    ;;"
             + "nro_doc_conyuge=VACIO;;"
             + "nombre_conyuge=VACIO;;"
             + "apellido_conyuge=VACIO;;"
             + "--POSIBLES_HERMANOS--;;"
             + "cedula=4.043.135;;"
             + "nombres=DIANA MARIA;;"
             + "apellidos=VILLALBA CACERES;;"
             + "lugar_nac=SAN PEDRO;;"
             + "fecha_nac=12/03/1984;;"
             + "edad=31;;"
             + "nacionalidad=PARAGUAYA;;"
             + "--POSIBLES_HERMANOS--;;"
             + "cedula=4.819.126;;"
             + "nombres=ELVIO GUSTAVO;;"
             + "apellidos=VILLALBA CACERES;;"
             + "lugar_nac=SAN PEDRO;;"
             + "fecha_nac=08/08/1990;;"
             + "edad=24;;"
             + "nacionalidad=PARAGUAYA;;"
             + "--PADRON_TSJE--;;"
             + "departamento=SAN PEDRO;;"
             + "distrito=SAN PEDRO DEL YCUAMANDYYU;;"
             + "zona=SAN PEDRO DEL YCUAMANDYYU;;"
             + "local=CENTRO FORMACION DOCENTE;;"
             + "fecha_inscripcion=2003-10-18;;"
             + "--TELEFONOS_DE_CONTACTO--;;"
             + "operador=TIGO;;"
             + "nro_cuenta=595982248210;;"
             + "billing=Tigo Prepago;;"
             + "nombres=EDGAR GILBERTO;;"
             + "apellidos=VILLALBA CACERES;;"
             + "fecha_activacion=2009-01-22;;"
             + "--TELEFONOS_DE_CONTACTO--;;"
             + "operador=PERSONAL;;"
             + "nro_cuenta=595972248210;;"
             + "billing=PERSONAL FACTURA;;"
             + "nombres=EDGAR GILBERTO;;"
             + "apellidos=VILLALBA CACERES;;"
             + "fecha_activacion=2019-01-22;;"
             + "--SET--;;"
             + "cedula=3664038;;"
             + "dv=7;;"
             + "contribuyente=VILLALBA CACERES, EDGAR GILBERTO;;"
             + "ruc_anterior=BAPA710231K;;"
             + "fecha_actualizacion=01/01/2015";
             int beginIndex = str.indexOf("--DATOS_PERSONA--");*/
            int beginIndex = str.indexOf("--RESULTADO--");
            int endIndex = str.indexOf("</body>");
            str = str.substring(beginIndex, endIndex);

            str = str.replaceAll("<br />", ";;").replaceAll("<br />", "").replaceAll("&deg;", "°").replaceAll("nro_doc_extrangero", "nro_doc_extranjero");
            str = str.replaceAll("<!--[A-Z]*--> *", "");
            str = str.replaceAll("<!--(.*?)-->", "");
           //  str = str.replaceAll("&lt;&gt;", "");
            
            //logger.info(str);
            //logger.info("CHAUUUUUUUUUUUU");
           // if(true)return null;
            String[] v = str.split(";;");
            str = "";
            for (String v1 : v) {
                str += v1.trim() + ";;";
            }
            logger.info(str);
            logger.info("CHAUUUU2");
           
            Parser p = new Parser();
            DatosPersona dp = p.parsear(str);
            if (dp != null && dp.getHermanos() !=null && dp.getHermanos().size() > 5) {
                dp.setHermanos(dp.getHermanos().subList(0, 5));
            }
             //logger.info(dp.getPdf_url());
            if(dp.getInformePdf()!=null && dp.getInformePdf().getPdf_url() != null){
                try{
                    dp.setInformePdf( extraerPDF(dp.getInformePdf().getPdf_url(),phpCookie));
                }catch(Exception e){
                    logger.error("No se pudo obtener el PDF");
                }
            }
            if(dp.getImagenRostro()!=null && dp.getImagenRostro().getPng_url()!= null){
                try{
                    dp.setImagenRostro(cargarImagen(dp.getImagenRostro().getPng_url(),phpCookie));
                }catch(Exception e){
                    logger.error("No se pudo obtener la imagens");
                }
            }
            
            
            responseWS = new BuscameDTO();
            responseWS.setDatosPersona(dp);
            responseWS.setEstado(new Integer(dp.getEstado()));
            responseWS.setMensaje(dp.getMensaje());
        } catch (Exception ex) {
            responseWS = new BuscameDTO();
            responseWS.setEstado(-1);
            responseWS.setMensaje(ex.getMessage());
            logger.error(ex.getMessage(), ex);
        }
        logger.info("OUT: [{}]", responseWS);
        return responseWS;
    }

    
    
    
    //http://www.tebuscamos.com.py/tebuscamos/theme/admin/personas_ws_bus.php?nombre=sergio&ape=staniche&nac=08/09/1984&padre=sergio&madre=sergia&conyuge=carolina&nacini=08/08/1984&nacfin=08/08/1984
    @WebMethod(operationName = "buscamePersona")
    public BuscameDTO buscamePersona(@WebParam(name = "usuario") String usuario, @WebParam(name = "password") String password, @WebParam(name = "nombre") String nombre
    , @WebParam(name = "nac") String nac
    , @WebParam(name = "padre") String padre
            , @WebParam(name = "madre") String madre
            , @WebParam(name = "conyuge") String conyuge
            , @WebParam(name = "nacini") String nacini
            , @WebParam(name = "nacfin") String nacfin
            
    ) {
        logger.info("IN: [usuario={}; password={}; nombre={} ; nac={}; padre={}; madre={}; conyuge={}; nacini={}; nacfin={}]", usuario, password, nombre,nac,padre,madre,conyuge,nacini,nacfin);
        BuscameDTO responseWS;
        try {
            HttpClient client = HttpClientBuilder.create().build();
            HttpPost post = new HttpPost(URL_LOGIN);
            post.setHeader("User-Agent", "Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.124 Safari/537.36");
            List<NameValuePair> urlParameters = new ArrayList<NameValuePair>();
            urlParameters.add(new BasicNameValuePair("user[email]", usuario));
            urlParameters.add(new BasicNameValuePair("user[pass]", password));
            post.setEntity(new UrlEncodedFormEntity(urlParameters));
            org.apache.http.HttpResponse response = client.execute(post);
            logger.info("Enviando 'POST' a URL : {}", URL_LOGIN);
            logger.info("Parametros POST : [{};{}]", usuario, password);
            logger.info("Response: [{}]", response.getStatusLine().getStatusCode());
            if(!(response.getStatusLine().getStatusCode()==200 || response.getStatusLine().getStatusCode()==302 )){
                              BuscameDTO b = new BuscameDTO();
                            b.setEstado(response.getStatusLine().getStatusCode());
                            b.setMensaje("ERROR HTTP");
                            return b;
                            
                            
            }
            String phpCookie = response.getFirstHeader("Set-Cookie").getValue();

            HttpGet get = new HttpGet(Constantes.URL_DATA_P + "nombre=" +nombre +"&nac="+nac+"&padre="+padre+"&madre="+madre+"&conyuge="+conyuge+"&nacini="+nacini+"&nacfin="+nacfin);
            get.setHeader("Cookie", phpCookie);
            response = client.execute(get);
            logger.info("Enviando 'GET' a URL : {}{}", Constantes.URL_DATA_P , "nombre=" +nombre +"&nac="+nac+"&padre="+padre+"&madre="+madre+"&conyuge="+conyuge+"&nacini="+nacini+"&nacfin="+nacfin);
            logger.info("Parametros GET : [{}]", phpCookie);
            logger.info("Response: [{}]", response.getStatusLine().getStatusCode());
            if(!(response.getStatusLine().getStatusCode()==200 || response.getStatusLine().getStatusCode()==302 )){
                              BuscameDTO b = new BuscameDTO();
                            b.setEstado(response.getStatusLine().getStatusCode());
                            b.setMensaje("ERROR HTTP");
                            return b;
                            
                            
            }

            BufferedReader br = new BufferedReader(new InputStreamReader(response.getEntity().getContent(), "UTF-8"));
            String output;
            String responseS = "";
            while ((output = br.readLine()) != null) {
                responseS += output;
            }

            Document doc = Jsoup.parseBodyFragment(responseS);
            String str = doc.body().toString();
            /*String str = "--DATOS_PERSONA--;;"
             + "estado=0;;"
             + "mensaje=exito;;"
             + "tipo=DNI;;"
             + "ultimo_registro_ips=holaaa;;"
             + "nacionalidad_origen=PARAGUAY;;"
             + "fecha_aprox_vto=01/10/2015;;"
             + "nro_documento=1.723.754;;"
             + "fecha_nac=08/09/1984;;"
             + "sexo=Masculino;;"
             + "estado_civ=SOLTERO/A;;"
             + "fecha_emision_ci=12/10/2009;;"
             + "domicilio=B° SANTA ANA - SAN PEDRO;;"
             + "nombre=EDGAR GILBERTO;;"
             + "edad=32;;"
             + "profesion=ESTUDIANTE;;"
             + "fecha_venc_ci=12/10/2019;;"
             + "barrio=SAN VICENTE;;"
             + "ciudad=ASUNCION;;"
             + "apellidos=VILLALBA CACERES;;"
             + "lugar=SAN PEDRO;;"
             + "nacionalidad=PARAGUAYA;;"
             + "nro_doc_extrangero=VACIO;;"
             + "fecha_grab_ci=05/10/2009;;"
             + "telefono=VACIO;;"
             + "--DATOS_FAMILIARES--;;"
             + "nro_doc_padre=VACIO;;"
             + "nombre_padre=ELVIO GILBERTO;;"
             + "apellido_padre=VACIO;;"
             + "nro_doc_madre=VACIO;;"
             + "nombre_madre=MARIA CESARIA;;"
             + "apellido_madre=VACIO    ;;"
             + "nro_doc_conyuge=VACIO;;"
             + "nombre_conyuge=VACIO;;"
             + "apellido_conyuge=VACIO;;"
             + "--POSIBLES_HERMANOS--;;"
             + "cedula=4.043.135;;"
             + "nombres=DIANA MARIA;;"
             + "apellidos=VILLALBA CACERES;;"
             + "lugar_nac=SAN PEDRO;;"
             + "fecha_nac=12/03/1984;;"
             + "edad=31;;"
             + "nacionalidad=PARAGUAYA;;"
             + "--POSIBLES_HERMANOS--;;"
             + "cedula=4.819.126;;"
             + "nombres=ELVIO GUSTAVO;;"
             + "apellidos=VILLALBA CACERES;;"
             + "lugar_nac=SAN PEDRO;;"
             + "fecha_nac=08/08/1990;;"
             + "edad=24;;"
             + "nacionalidad=PARAGUAYA;;"
             + "--PADRON_TSJE--;;"
             + "departamento=SAN PEDRO;;"
             + "distrito=SAN PEDRO DEL YCUAMANDYYU;;"
             + "zona=SAN PEDRO DEL YCUAMANDYYU;;"
             + "local=CENTRO FORMACION DOCENTE;;"
             + "fecha_inscripcion=2003-10-18;;"
             + "--TELEFONOS_DE_CONTACTO--;;"
             + "operador=TIGO;;"
             + "nro_cuenta=595982248210;;"
             + "billing=Tigo Prepago;;"
             + "nombres=EDGAR GILBERTO;;"
             + "apellidos=VILLALBA CACERES;;"
             + "fecha_activacion=2009-01-22;;"
             + "--TELEFONOS_DE_CONTACTO--;;"
             + "operador=PERSONAL;;"
             + "nro_cuenta=595972248210;;"
             + "billing=PERSONAL FACTURA;;"
             + "nombres=EDGAR GILBERTO;;"
             + "apellidos=VILLALBA CACERES;;"
             + "fecha_activacion=2019-01-22;;"
             + "--SET--;;"
             + "cedula=3664038;;"
             + "dv=7;;"
             + "contribuyente=VILLALBA CACERES, EDGAR GILBERTO;;"
             + "ruc_anterior=BAPA710231K;;"
             + "fecha_actualizacion=01/01/2015";
             int beginIndex = str.indexOf("--DATOS_PERSONA--");*/
            int beginIndex = str.indexOf("--DATOS_PERSONA--");
            int endIndex = str.indexOf("</body>");
            str = str.substring(beginIndex, endIndex);

            str = str.replaceAll("<br />", ";;").replaceAll("<br />", "").replaceAll("&deg;", "°").replaceAll("nro_doc_extrangero", "nro_doc_extranjero");
            str = str.replaceAll("<!--[A-Z]*--> *", "");
            logger.info(str);
            String[] v = str.split(";;");
            str = "";
            for (String v1 : v) {
                str += v1.trim() + ";;";
            }

            Parser p = new Parser();
            DatosPersona dp = p.parsear(str);
            if (dp != null && dp.getHermanos() !=null && dp.getHermanos().size() > 5) {
                dp.setHermanos(dp.getHermanos().subList(0, 5));
            }
             //logger.info(dp.getPdf_url());
            if(dp.getInformePdf()!=null && dp.getInformePdf().getPdf_url() != null){
                try{
                    dp.setInformePdf( extraerPDF(dp.getInformePdf().getPdf_url(),phpCookie));
                }catch(Exception e){
                    logger.error("No se pudo obtener el PDF");
                }
            }
            responseWS = new BuscameDTO();
            responseWS.setDatosPersona(dp);
           responseWS.setEstado(new Integer(dp.getEstado()));
            responseWS.setMensaje(dp.getMensaje());
        } catch (Exception ex) {
            responseWS = new BuscameDTO();
            responseWS.setEstado(-1);
            responseWS.setMensaje(ex.getMessage());
            logger.error(ex.getMessage(), ex);
        }
        logger.info("OUT: [{}]", responseWS);
        return responseWS;
    }
    
      @WebMethod(operationName = "buscameTelefono")
    public BuscameDTO buscameelefono(@WebParam(name = "usuario") String usuario, @WebParam(name = "password") String password, @WebParam(name = "telefono") String telefono) {
        logger.info("IN: [usuario={}; password={}; telefono={}]", usuario, password, telefono);
        BuscameDTO responseWS;
        try {
            HttpClient client = HttpClientBuilder.create().build();
            HttpPost post = new HttpPost(URL_LOGIN);
            post.setHeader("User-Agent", "Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.124 Safari/537.36");
            List<NameValuePair> urlParameters = new ArrayList<NameValuePair>();
            urlParameters.add(new BasicNameValuePair("user[email]", usuario));
            urlParameters.add(new BasicNameValuePair("user[pass]", password));
            post.setEntity(new UrlEncodedFormEntity(urlParameters));
            org.apache.http.HttpResponse response = client.execute(post);
            logger.info("Enviando 'POST' a URL : {}", URL_LOGIN);
            logger.info("Parametros POST : [{};{}]", usuario, password);
            logger.info("Response: [{}]", response.getStatusLine().getStatusCode());
            if(!(response.getStatusLine().getStatusCode()==200 || response.getStatusLine().getStatusCode()==302 )){
                              BuscameDTO b = new BuscameDTO();
                            b.setEstado(response.getStatusLine().getStatusCode());
                            b.setMensaje("ERROR HTTP");
                            return b;
                            
                            
            }
            String phpCookie = response.getFirstHeader("Set-Cookie").getValue();

            HttpGet get = new HttpGet(Constantes.URL_DATA_T + telefono);
            get.setHeader("Cookie", phpCookie);
            response = client.execute(get);
            logger.info("Enviando 'GET' a URL : {}{}", Constantes.URL_DATA_T, telefono);
            logger.info("Parametros GET : [{}]", phpCookie);
            logger.info("Response: [{}]", response.getStatusLine().getStatusCode());
            if(!(response.getStatusLine().getStatusCode()==200 || response.getStatusLine().getStatusCode()==302 )){
                              BuscameDTO b = new BuscameDTO();
                            b.setEstado(response.getStatusLine().getStatusCode());
                            b.setMensaje("ERROR HTTP");
                            return b;
                            
                            
            }

            BufferedReader br = new BufferedReader(new InputStreamReader(response.getEntity().getContent(), "UTF-8"));
            String output;
            String responseS = "";
            while ((output = br.readLine()) != null) {
                responseS += output;
            }

            Document doc = Jsoup.parseBodyFragment(responseS);
            String str = doc.body().toString();
            /*String str = "--DATOS_PERSONA--;;"
             + "estado=0;;"
             + "mensaje=exito;;"
             + "tipo=DNI;;"
             + "ultimo_registro_ips=holaaa;;"
             + "nacionalidad_origen=PARAGUAY;;"
             + "fecha_aprox_vto=01/10/2015;;"
             + "nro_documento=1.723.754;;"
             + "fecha_nac=08/09/1984;;"
             + "sexo=Masculino;;"
             + "estado_civ=SOLTERO/A;;"
             + "fecha_emision_ci=12/10/2009;;"
             + "domicilio=B° SANTA ANA - SAN PEDRO;;"
             + "nombre=EDGAR GILBERTO;;"
             + "edad=32;;"
             + "profesion=ESTUDIANTE;;"
             + "fecha_venc_ci=12/10/2019;;"
             + "barrio=SAN VICENTE;;"
             + "ciudad=ASUNCION;;"
             + "apellidos=VILLALBA CACERES;;"
             + "lugar=SAN PEDRO;;"
             + "nacionalidad=PARAGUAYA;;"
             + "nro_doc_extrangero=VACIO;;"
             + "fecha_grab_ci=05/10/2009;;"
             + "telefono=VACIO;;"
             + "--DATOS_FAMILIARES--;;"
             + "nro_doc_padre=VACIO;;"
             + "nombre_padre=ELVIO GILBERTO;;"
             + "apellido_padre=VACIO;;"
             + "nro_doc_madre=VACIO;;"
             + "nombre_madre=MARIA CESARIA;;"
             + "apellido_madre=VACIO    ;;"
             + "nro_doc_conyuge=VACIO;;"
             + "nombre_conyuge=VACIO;;"
             + "apellido_conyuge=VACIO;;"
             + "--POSIBLES_HERMANOS--;;"
             + "cedula=4.043.135;;"
             + "nombres=DIANA MARIA;;"
             + "apellidos=VILLALBA CACERES;;"
             + "lugar_nac=SAN PEDRO;;"
             + "fecha_nac=12/03/1984;;"
             + "edad=31;;"
             + "nacionalidad=PARAGUAYA;;"
             + "--POSIBLES_HERMANOS--;;"
             + "cedula=4.819.126;;"
             + "nombres=ELVIO GUSTAVO;;"
             + "apellidos=VILLALBA CACERES;;"
             + "lugar_nac=SAN PEDRO;;"
             + "fecha_nac=08/08/1990;;"
             + "edad=24;;"
             + "nacionalidad=PARAGUAYA;;"
             + "--PADRON_TSJE--;;"
             + "departamento=SAN PEDRO;;"
             + "distrito=SAN PEDRO DEL YCUAMANDYYU;;"
             + "zona=SAN PEDRO DEL YCUAMANDYYU;;"
             + "local=CENTRO FORMACION DOCENTE;;"
             + "fecha_inscripcion=2003-10-18;;"
             + "--TELEFONOS_DE_CONTACTO--;;"
             + "operador=TIGO;;"
             + "nro_cuenta=595982248210;;"
             + "billing=Tigo Prepago;;"
             + "nombres=EDGAR GILBERTO;;"
             + "apellidos=VILLALBA CACERES;;"
             + "fecha_activacion=2009-01-22;;"
             + "--TELEFONOS_DE_CONTACTO--;;"
             + "operador=PERSONAL;;"
             + "nro_cuenta=595972248210;;"
             + "billing=PERSONAL FACTURA;;"
             + "nombres=EDGAR GILBERTO;;"
             + "apellidos=VILLALBA CACERES;;"
             + "fecha_activacion=2019-01-22;;"
             + "--SET--;;"
             + "cedula=3664038;;"
             + "dv=7;;"
             + "contribuyente=VILLALBA CACERES, EDGAR GILBERTO;;"
             + "ruc_anterior=BAPA710231K;;"
             + "fecha_actualizacion=01/01/2015";
             int beginIndex = str.indexOf("--DATOS_PERSONA--");*/
            int beginIndex = str.indexOf("--DATOS_PERSONA--");
            int endIndex = str.indexOf("</body>");
            str = str.substring(beginIndex, endIndex);

            str = str.replaceAll("<br />", ";;").replaceAll("<br />", "").replaceAll("&deg;", "°").replaceAll("nro_doc_extrangero", "nro_doc_extranjero");
            str = str.replaceAll("<!--[A-Z]*--> *", "");
            logger.info(str);
            String[] v = str.split(";;");
            str = "";
            for (String v1 : v) {
                str += v1.trim() + ";;";
            }

            Parser p = new Parser();
            DatosPersona dp = p.parsear(str);
            if (dp != null && dp.getHermanos() !=null && dp.getHermanos().size() > 5) {
                dp.setHermanos(dp.getHermanos().subList(0, 5));
            }
            //logger.info(dp.getPdf_url());
            if(dp.getInformePdf()!=null && dp.getInformePdf().getPdf_url() != null){
                try{
                    dp.setInformePdf( extraerPDF(dp.getInformePdf().getPdf_url(),phpCookie));
                }catch(Exception e){
                    logger.error("No se pudo obtener el PDF");
                }
            }
            responseWS = new BuscameDTO();
            responseWS.setDatosPersona(dp);
             responseWS.setEstado(new Integer(dp.getEstado()));
            responseWS.setMensaje(dp.getMensaje());
        } catch (Exception ex) {
            responseWS = new BuscameDTO();
            responseWS.setEstado(-1);
            responseWS.setMensaje(ex.getMessage());
            logger.error(ex.getMessage(), ex);
        }
        logger.info("OUT: [{}]", responseWS);
        return responseWS;
    }
    
    /**
     *
     * @param usuario
     * @param password
     * @param cedula
     * @return
     */
    @WebMethod(operationName = "buscamePDF")
    public InformePdf buscamePDF(@WebParam(name = "usuario") String usuario, @WebParam(name = "password") String password, @WebParam(name = "cedula") String cedula,@WebParam(name = "tipo") String tipo) {
        logger.info("IN: [usuario={}; password={}; cedula={}; tipo=()]", usuario, password, cedula, tipo);
        BuscameDTO responseWS ;
        if(tipo != null && !tipo.equals("basic") && !tipo.equals("full")){
            BuscameDTO b = new BuscameDTO();
            b.setEstado(-1);
            b.setMensaje("error de parametro tipo");
        }
        
        if(tipo==null) tipo = "full";
        
        
        try {
            HttpClient client = HttpClientBuilder.create().build();
            HttpPost post = new HttpPost(URL_LOGIN);
            post.setHeader("User-Agent", "Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.124 Safari/537.36");
            List<NameValuePair> urlParameters = new ArrayList<NameValuePair>();
            urlParameters.add(new BasicNameValuePair("user[email]", usuario));
            urlParameters.add(new BasicNameValuePair("user[pass]", password));
            post.setEntity(new UrlEncodedFormEntity(urlParameters));
            org.apache.http.HttpResponse response = client.execute(post);
            logger.info("Enviando 'POST' a URL : {}", URL_LOGIN);
            logger.info("Parametros POST : [{};{}]", usuario, password);
            logger.info("Response: [{}]", response.getStatusLine().getStatusCode());
            if(!(response.getStatusLine().getStatusCode()==200 || response.getStatusLine().getStatusCode()==302 )){
                              BuscameDTO b = new BuscameDTO();
                            b.setEstado(response.getStatusLine().getStatusCode());
                            b.setMensaje("ERROR HTTP");
                          //  return b;
                            
                            
            }
            String phpCookie = response.getFirstHeader("Set-Cookie").getValue();
            String url = URL_DATA2.replaceAll("<cedula>", cedula).replaceAll("<tipo>", tipo);
            HttpGet get = new HttpGet(url);
           //HttpGet get = new HttpGet(URL_DATA3);
           get.setHeader("Cookie", phpCookie);
            response = client.execute(get);
            logger.info("Enviando 'GET' a URL : {}{}", url, cedula);
            logger.info("Parametros GET : [{}]", phpCookie);
            logger.info("Response: [{}]", response.getStatusLine().getStatusCode());
            if(!(response.getStatusLine().getStatusCode()==200 || response.getStatusLine().getStatusCode()==302 )){
                              BuscameDTO b = new BuscameDTO();
                            b.setEstado(response.getStatusLine().getStatusCode());
                            b.setMensaje("ERROR HTTP");
                            
                            
            }

            BufferedReader br = new BufferedReader(new InputStreamReader(response.getEntity().getContent(), "UTF-8"));
            String output;
            String responseS = "";
            while ((output = br.readLine()) != null) {
                responseS += output;
            }

            Document doc = Jsoup.parseBodyFragment(responseS);
            String str = doc.body().toString();
            /*String str = "--DATOS_PERSONA--;;"
             + "estado=0;;"
             + "mensaje=exito;;"
             + "tipo=DNI;;"
             + "ultimo_registro_ips=holaaa;;"
             + "nacionalidad_origen=PARAGUAY;;"
             + "fecha_aprox_vto=01/10/2015;;"
             + "nro_documento=1.723.754;;"
             + "fecha_nac=08/09/1984;;"
             + "sexo=Masculino;;"
             + "estado_civ=SOLTERO/A;;"
             + "fecha_emision_ci=12/10/2009;;"
             + "domicilio=B° SANTA ANA - SAN PEDRO;;"
             + "nombre=EDGAR GILBERTO;;"
             + "edad=32;;"
             + "profesion=ESTUDIANTE;;"
             + "fecha_venc_ci=12/10/2019;;"
             + "barrio=SAN VICENTE;;"
             + "ciudad=ASUNCION;;"
             + "apellidos=VILLALBA CACERES;;"
             + "lugar=SAN PEDRO;;"
             + "nacionalidad=PARAGUAYA;;"
             + "nro_doc_extrangero=VACIO;;"
             + "fecha_grab_ci=05/10/2009;;"
             + "telefono=VACIO;;"
             + "--DATOS_FAMILIARES--;;"
             + "nro_doc_padre=VACIO;;"
             + "nombre_padre=ELVIO GILBERTO;;"
             + "apellido_padre=VACIO;;"
             + "nro_doc_madre=VACIO;;"
             + "nombre_madre=MARIA CESARIA;;"
             + "apellido_madre=VACIO    ;;"
             + "nro_doc_conyuge=VACIO;;"
             + "nombre_conyuge=VACIO;;"
             + "apellido_conyuge=VACIO;;"
             + "--POSIBLES_HERMANOS--;;"
             + "cedula=4.043.135;;"
             + "nombres=DIANA MARIA;;"
             + "apellidos=VILLALBA CACERES;;"
             + "lugar_nac=SAN PEDRO;;"
             + "fecha_nac=12/03/1984;;"
             + "edad=31;;"
             + "nacionalidad=PARAGUAYA;;"
             + "--POSIBLES_HERMANOS--;;"
             + "cedula=4.819.126;;"
             + "nombres=ELVIO GUSTAVO;;"
             + "apellidos=VILLALBA CACERES;;"
             + "lugar_nac=SAN PEDRO;;"
             + "fecha_nac=08/08/1990;;"
             + "edad=24;;"
             + "nacionalidad=PARAGUAYA;;"
             + "--PADRON_TSJE--;;"
             + "departamento=SAN PEDRO;;"
             + "distrito=SAN PEDRO DEL YCUAMANDYYU;;"
             + "zona=SAN PEDRO DEL YCUAMANDYYU;;"
             + "local=CENTRO FORMACION DOCENTE;;"
             + "fecha_inscripcion=2003-10-18;;"
             + "--TELEFONOS_DE_CONTACTO--;;"
             + "operador=TIGO;;"
             + "nro_cuenta=595982248210;;"
             + "billing=Tigo Prepago;;"
             + "nombres=EDGAR GILBERTO;;"
             + "apellidos=VILLALBA CACERES;;"
             + "fecha_activacion=2009-01-22;;"
             + "--TELEFONOS_DE_CONTACTO--;;"
             + "operador=PERSONAL;;"
             + "nro_cuenta=595972248210;;"
             + "billing=PERSONAL FACTURA;;"
             + "nombres=EDGAR GILBERTO;;"
             + "apellidos=VILLALBA CACERES;;"
             + "fecha_activacion=2019-01-22;;"
             + "--SET--;;"
             + "cedula=3664038;;"
             + "dv=7;;"
             + "contribuyente=VILLALBA CACERES, EDGAR GILBERTO;;"
             + "ruc_anterior=BAPA710231K;;"
             + "fecha_actualizacion=01/01/2015";
             int beginIndex = str.indexOf("--DATOS_PERSONA--");*/
            int beginIndex = str.indexOf("--DATOS_PERSONA--");
            int endIndex = str.indexOf("</body>");
            str = str.substring(beginIndex, endIndex);

            str = str.replaceAll("<br />", ";;").replaceAll("<br />", "").replaceAll("&deg;", "°").replaceAll("nro_doc_extrangero", "nro_doc_extranjero");
            str = str.replaceAll("<!--[A-Z]*--> *", "");
            str = str.replaceAll("<!--(.*?)-->", "");
           //  str = str.replaceAll("&lt;&gt;", "");
            
            //logger.info(str);
            //logger.info("CHAUUUUUUUUUUUU");
           // if(true)return null;
            String[] v = str.split(";;");
            str = "";
            for (String v1 : v) {
                str += v1.trim() + ";;";
            }
            logger.info(str);
            logger.info("CHAUUUU2");
           
            Parser p = new Parser();
            DatosPersona2 dp = p.parser2(str);
            if (dp != null && dp.getHermanos() !=null && dp.getHermanos().size() > 5) {
                dp.setHermanos(dp.getHermanos().subList(0, 5));
            }
            responseWS = new BuscameDTO();
            //responseWS.setDatosPersona(dp);
            responseWS.setEstado(new Integer(dp.getEstado()));
            responseWS.setMensaje(dp.getMensaje());
            logger.info(dp.getPdf_url());
            InformePdf pp = extraerPDF(dp.getPdf_url(),phpCookie);
            
            return pp; 
        } catch (Exception ex) {
            responseWS = new BuscameDTO();
            responseWS.setEstado(-1);
            responseWS.setMensaje(ex.getMessage());
            logger.error(ex.getMessage(), ex);
        }
        logger.info("OUT: [{}]", responseWS);
        
       
       
        
        return null;
    }
    
    InformePdf extraerPDF(String pdfurl, String phpCookie) throws Exception{
    
      String res="";   
      InformePdf pdf = new  InformePdf();
      pdf.setPdf_url(pdfurl);
      //comentar para probar en desarrollo
      pdfurl = pdfurl.replaceAll("www\\.tebuscamos\\.com\\.py", "localhost");
    //  List<Byte> =new ArrayList<Byte>();
    List<Integer> inte = new ArrayList<>();
     HttpResponse<String> response2 = Unirest.get(pdfurl)
  .header("User-Agent", "PostmanRuntime/7.19.0")
  .header("Accept", "*/*")
  .header("Cache-Control", "no-cache")
  //.header("Postman-Token", "3217e264-f2fc-4944-9c53-e451dc5caccd,fc5ff0b8-86d5-4ad9-a8fb-98c2e9df3b25")
  .header("Host", "www.tebuscamos.com.py")
  .header("Accept-Encoding", "gzip, deflate")
  .header("Cookie", phpCookie)
  .header("Connection", "keep-alive")
  .header("cache-control", "no-cache")
  .asString();
                   
               
             //  System.out.println(response2.getRawBody());
              //   OutputStream out = new FileOutputStream("out.pdf");
              
             String nombre= response2.getHeaders().get("content-disposition");
             System.out.println(nombre);
             Pattern pt = Pattern.compile(".*\\\"(.*)\\\".*");
             Matcher m = pt.matcher(nombre);
             if(m.find()) {
                 nombre = m.group(1);
             }
             System.out.println(nombre);
              //  content-disposition inline; filename="1234567890-LEYVIS LUIS-VALDES LOPEZ.pdf"]]

                
               int i;
               while((i = response2.getRawBody().read())!=-1) {
         
            // converts integer to character
          
            char c = (char)i;
            inte.add(i);
             //out.write(i);
            // prints character
           // System.out.print(c);
           
         }
           // out.close();
           byte[]  puro = new byte[inte.size()] ;    
          for(int p = 0; p < inte.size(); p++)      
            puro[p] = (byte)(int) inte.get(p);      
               
               
                 
            
          
  
          byte[] ba1 = Base64.encode(puro);
          pdf.setContenido(ba1);
          pdf.setNombrearchivo(nombre);
          
        return  pdf;
    }
    
    
    ImagenRostro cargarImagen(String url, String phpCookie){
        ImagenRostro img = new ImagenRostro();
        
        
        
            //ahora esta asi pq no hay imagen
           //  url = URL_DATA_IMAGEN;
           //para desarrollo se debe comentar
          url = url.replaceAll("lesa\\.cba\\.com\\.py", "localhost");
               this.logger.info("url:"+url); 
      try{
               HttpClient client = HttpClientBuilder.create().build();
            HttpGet get = new HttpGet(url);
           //HttpGet get = new HttpGet(URL_DATA3);
           get.setHeader("Cookie", phpCookie);
           get.setHeader("charset","utf-8");
           get.setHeader(null);
           org.apache.http.HttpResponse  response = client.execute(get);
           // logger.info("Enviando 'GET' a URL : {}{}", url, cedula);
            logger.info("Parametros GET : [{}]", phpCookie);
            logger.info("Response: [{}]", response.getStatusLine().getStatusCode());
            if(!(response.getStatusLine().getStatusCode()==200 || response.getStatusLine().getStatusCode()==302 ) ){
                              BuscameDTO b = new BuscameDTO();
                            b.setEstado(response.getStatusLine().getStatusCode());
                            b.setMensaje("ERROR HTTP");
                            return img;
                            
                            
            }
            BufferedReader br = new BufferedReader(new InputStreamReader(response.getEntity().getContent(), "UTF-8"));
            String output;
            String responseS = "";
            while ((output = br.readLine()) != null) {
                responseS += output;
            }
            

            
            ///para probar si no hace falta encriptar
            


            

            URL url2 = new URL(url);
            URLConnection urlConn = url2.openConnection();
            urlConn.setRequestProperty("Cookie", phpCookie);
            BufferedImage bImage2 = ImageIO.read(url2);
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ImageIO.write(bImage2, "png", baos);
            byte[] bytes = baos.toByteArray();
            byte[] ba1 = Base64.encode(bytes);
            img.setContenido(ba1);
            img.setPng_url(url);
            String[] lista = url.split("/");
           
           //para sacar el nombre 
            img.setNombrearchivo(lista[lista.length-1]);
            //
            
        
        
      }catch(Exception e){
          this.logger.error("ERROR FATAL",e);
      }
        
        
        return img;
    }
    
    
}
